package com.carbon.tracker;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.carbon.tracker.db.xml.XML;
import com.carbon.tracker.entity.Entity;
import com.carbon.tracker.entity.User;
import com.carbon.tracker.entity.UserSettings;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

public class CarbonTrackerActivity extends Activity implements LocationListener {
    private int gps=0;
	Dialog dialog;
	
	
    /** Called when the activity is first created. */	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
       
        ImageView button = (ImageView)findViewById(R.id.imageView_car);
        button.setOnClickListener(startTrack);
        
        ImageView button2 = (ImageView)findViewById(R.id.imageView_map);
        button2.setOnClickListener(startCompare);
        
        if(Constants.firstTime){
        	showDialoge();
        	Constants.firstTime=false;
        }
    }
    
    
    private void showDialoge() {
    	dialog = new Dialog(this);
	    
    	dialog.setContentView(R.layout.settings_custom_dialog);
		dialog.setTitle(R.string.menu_settings);
		dialog.setCancelable(false);
		Spinner spinner = (Spinner) dialog.findViewById(R.id.spinner_cus_dial_fuel);
	    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
	            this, R.array.fuelArray, android.R.layout.simple_spinner_item);
	    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    spinner.setAdapter(adapter);
		
		Button button = (Button) dialog.findViewById(R.id.button_settings_submit);
		button.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				EditText manufactere = (EditText) dialog.findViewById(R.id.editText_cus_dial_manuf);
				EditText modelNam = (EditText) dialog.findViewById(R.id.editText_cus_dial_model);
				EditText cc = (EditText) dialog.findViewById(R.id.editText_cus_dial_ccm);
				Spinner spinner = (Spinner) dialog.findViewById(R.id.spinner_cus_dial_fuel);
				if(!(manufactere.length() < 1 || modelNam.length() < 1 ||  cc.length() < 1)){
					String manufacterer = manufactere.getText().toString();
					String modelName = modelNam.getText().toString();
					String ccm = cc.getText().toString();
					String fuel = spinner.getSelectedItem().toString();
					XML xmlObj = new XML(CarbonTrackerActivity.this);
					//TODO pogledat na bazo pa dobit grams per km
					
					ResultSet rs = getGramsPerKm(manufacterer, modelName, ccm, fuel, "%");
					Double gramPerKm = null;	
					try {
						if (rs != null){
							
							gramPerKm = (double) rs.getInt(15);
							xmlObj.addSettings(manufacterer, modelName, ccm, fuel, "%", Constants.SYNC_ON_END_ROAD, gramPerKm);
							Entity.closeConnection();
						}
						else {
							Log.d("error", "grami 0.0");
							dialogEnterConsuption();
							
						}
					} catch (SQLException e) {
						e.printStackTrace();
						// isto kot v else... pop-up dialog
						dialogEnterConsuption(); 
						}
					
					
					
					dialog.dismiss();
				}else{
					Toast.makeText(CarbonTrackerActivity.this, "Blank fields", Toast.LENGTH_SHORT).show();
				}
			}
			
			public ResultSet getGramsPerKm(String manufacturer, String modelName, String ccm, String fuel, String mkNum) {
				
				if(ccm.contains(","))
					ccm = ccm.replace(",", "");
				if(ccm.contains("."))
					ccm = ccm.replace(".", "");
				
				int ccm1 = Integer.parseInt(ccm) - 100;
				if(ccm1 < 100)
					ccm1 *= 100;
				int ccm2 = Integer.parseInt(ccm) + 100;
				if(ccm2 < 100)
					ccm2 *= 100;
				
				String sql = "SELECT * FROM passenger_cars WHERE man LIKE '"+manufacturer+"' AND Cn LIKE '"+modelName+"' AND `Ec (cm3)` BETWEEN "+ccm1+" AND "+ccm2+" AND Ft='"+fuel+"' AND Va='"+mkNum+"'";
				Statement stmt;
				ResultSet rs=null; 
				try {
					if (Constants.CONN.isClosed()) {
						Constants.CONN = Entity.getConnection();
					}
					stmt = Constants.CONN.createStatement();
					
					rs = stmt.executeQuery(sql);
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				try {
					if (rs != null)
						rs.first();
					
				} catch (SQLException e) {
					
					rs = null;
					e.printStackTrace();
				}
				
				return rs;
				
			}
		});
		
		User u = Constants.user;
		UserSettings us = new UserSettings();
//		us.getConnection();
		us.select(u.getUserId());
//		us.closeConnection();
		
		if(us.getId() == -1)
			dialog.show();
		else{
			XML xml = new XML(this);
			us.setUserId(u.getUserId());
			xml.addSettings(us);
		}
	}
    public void dialogEnterConsuption(){
    	
    	LayoutInflater factory = LayoutInflater.from(CarbonTrackerActivity.this);
		final View textEntryView = factory.inflate(R.layout.enter_consuption, null);
		AlertDialog.Builder builder = new AlertDialog.Builder(CarbonTrackerActivity.this);
		builder.setTitle(R.string.consuption);
		builder.setCancelable(false);
        builder.setView(textEntryView);
        builder.setPositiveButton(R.string.submit, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                	double consup = 0;
                	EditText petrolLiter = (EditText)textEntryView.findViewById(R.id.petrol_liter);
                	EditText petrolGalon = (EditText)textEntryView.findViewById(R.id.petrol_galon);
                	EditText dieselLiter = (EditText)textEntryView.findViewById(R.id.diesel_liter);
                	EditText diesleGalon = (EditText)textEntryView.findViewById(R.id.diesel_galon);
                	XML xmlObj = new XML(CarbonTrackerActivity.this);
                	if(petrolLiter.getText().length() != 0){
                		consup =  Double.parseDouble(petrolLiter.getText().toString());
                		consup /= 100;
                		consup *= 2.331;
                	}else if(petrolGalon.getText().toString().length() != 0){
                		consup =  Double.parseDouble(petrolGalon.getText().toString());
                		consup /= 100;
                		consup *= 8.824;
                	}else if(dieselLiter.getText().toString().length()!=0){
                		String a = dieselLiter.getText().toString();
                		consup =  Double.parseDouble(a);
                		consup /= 100;
                		consup *= 2.772;
                	}else if(diesleGalon.getText().toString().length()!=0){
                		consup =  Double.parseDouble(diesleGalon.getText().toString());
                		consup /= 100;
                		consup *= 10.493;
                	}
                	xmlObj.addSettings("notFound", "notFound", "0", "notFound", "%", Constants.SYNC_ON_END_ROAD, consup);

                	Entity.closeConnection();
                		
                }
            });
        AlertDialog alert=builder.create();
		alert.show();
    }
    @Override
    protected void onResume() {
    	super.onResume();
		if(gps<2){
	        try {
				// get location manager to check gps availability
	        	LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
				locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000L,1.0f, this);
			
				boolean isGPS = locationManager.isProviderEnabled (LocationManager.GPS_PROVIDER); 
	
				if(!isGPS){
					gps++;
					if(gps<2) checkGps();
					else finish();
				}
				else{
					gps=2;				
					//gps is available, do actions here		    			        
				}				
			
		 	} catch (Exception e1) {
		 		gps++;
		 		if(gps<2) checkGps();
		 		else finish();
			}
        }
    }
    public void checkGps(){
		final CharSequence[] items = {"Enable gps ", "Exit"};
		AlertDialog.Builder builder = new AlertDialog.Builder(CarbonTrackerActivity.this);
		builder.setCancelable(false);
		builder.setTitle(R.string.No_gps);
		builder.setItems(items, new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				//torej ravnaj se po int which.. ce je 0 = statistic, 1=exit
				
				if(which==0){
					startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
				}else{
					finish();						
				}		
			}
		});
		AlertDialog alert=builder.create();
		alert.show();
    }


	private OnClickListener startTrack = new OnClickListener() {
        public void onClick(View v) {
        	
        	Intent mapIntent = new Intent(CarbonTrackerActivity.this, GoogleMapsActivity.class);

        	Bundle buttStart = new Bundle();
        	buttStart.putInt("startMode", Constants.START_MODE_TRACK);
        	mapIntent.putExtras(buttStart);
        	
        	CarbonTrackerActivity.this.startActivity(mapIntent);

        }
    };
    
    private OnClickListener startCompare = new OnClickListener() {
        public void onClick(View v) {
        	
        	Intent mapIntent = new Intent(CarbonTrackerActivity.this, GoogleMapsActivity.class);

        	Bundle buttStart = new Bundle();
        	buttStart.putInt("startMode", Constants.START_MODE_DES_LOC);
        	mapIntent.putExtras(buttStart);
        	
        	CarbonTrackerActivity.this.startActivity(mapIntent);

        }
    };
    /*
     * nujno zlo (metode, ki jih moramo imet zaradi GPSlistenera :)
     * (non-Javadoc)
     * @see android.location.LocationListener#onLocationChanged(android.location.Location)
     */
	public void onLocationChanged(Location location) {}
	public void onProviderDisabled(String provider) {}
	public void onProviderEnabled(String provider) {}
	public void onStatusChanged(String provider, int status, Bundle extras) {}   
    
    
}