package com.carbon.tracker.db.xml;
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
 
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.io.*; 
import org.w3c.dom.*; 
import javax.xml.transform.*; 
import java.io.IOException;
import java.util.LinkedList;

import org.xml.sax.SAXException;

import android.content.Context;
import android.util.Log;

import com.carbon.tracker.Constants;
import com.carbon.tracker.entity.CarbonData;
import com.carbon.tracker.entity.MapPoint;
import com.carbon.tracker.entity.Road;
import com.carbon.tracker.entity.User;
import com.carbon.tracker.entity.UserSettings;

import javax.xml.xpath.*;
 
public class XML {
	
	public static DocumentBuilderFactory docFactory;
	public static DocumentBuilder docBuilder;
	public static Document doc;
	public static Element rootElement;
	public static Element Users;
	public Context con;
	
	public XML(Context context){
	
		try{
			
			File file = context.getFileStreamPath("baza.xml");
			
			this.con = context;
			
			if(!file.exists())
				this.init();
			
		}catch(Exception e){
			Log.d("error", e.toString());
		}
	}
	
	public void initDoc(){
		
		File file = con.getFileStreamPath("baza.xml");
		
		try{
			
			docFactory = DocumentBuilderFactory.newInstance();
			docBuilder = docFactory.newDocumentBuilder();
		    
		    doc = docBuilder.parse(file);
		    
			if(doc == null)
				doc = docBuilder.newDocument();
			
		}catch(Exception e){
			Log.d("error", e.toString());
			try {
				file.delete();
				this.init();
				this.initDoc();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public void init() throws XPathExpressionException, ParserConfigurationException, SAXException, IOException { 
				
		docFactory = DocumentBuilderFactory.newInstance();
		docBuilder = docFactory.newDocumentBuilder();
		
		doc = docBuilder.newDocument();
		
		rootElement = doc.createElement("baza");
		doc.appendChild(rootElement);
		
		// User elements
		Users = doc.createElement("Users");
		rootElement.appendChild(Users);
 
		// carbonData elements
		Element carbonData = doc.createElement("carbonData");
		rootElement.appendChild(carbonData);
		
		// Road elements
		Element Road = doc.createElement("Road");
		rootElement.appendChild(Road);
		
		// TravelMode elements
		Element TravelMode = doc.createElement("TravelMode");
		rootElement.appendChild(TravelMode);
		
		// MapPoints
		Element MapPoints = doc.createElement("MapPoints");
		rootElement.appendChild(MapPoints);
		
		// Settings
		Element settings = doc.createElement("Settings");
		rootElement.appendChild(settings);
		
		saveToXml();
	}
	
	public UserSettings selectSettings() throws XPathExpressionException{
		UserSettings setting = new UserSettings();
		initDoc();
		
		NodeList nl = doc.getElementsByTagName("Settings");
        Node b13Node = (Node) nl.item(0);
        
        NodeList children = b13Node.getChildNodes();
        //TODO prebrat iz xmlja settingse
        XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    XPathExpression exprManufacturer = xpath.compile("//Settings/Setting/manufacturer/text()");
	    XPathExpression exprUserId = xpath.compile("//Settings/Setting/userId/text()");
	    XPathExpression exprModelName = xpath.compile("//Settings/Setting/modelName/text()");
	    XPathExpression exprCcm = xpath.compile("//Settings/Setting/ccm/text()");
	    XPathExpression exprFuel = xpath.compile("//Settings/Setting/fuel/text()");
	    XPathExpression exprMakeNum = xpath.compile("//Settings/Setting/makeNum/text()");
	    XPathExpression exprOnSync = xpath.compile("//Settings/Setting/onSync/text()");
	    XPathExpression exprGramsPerKm = xpath.compile("//Settings/Setting/gramsPerKm/text()");
        
	    Object resultManufacturer = exprManufacturer.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesManufacturer = (NodeList) resultManufacturer;
	    
	    Object resultUserId = exprUserId.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesUserId = (NodeList) resultUserId;
	    
	    Object resultModelName = exprModelName.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesModelName = (NodeList) resultModelName;
	    
	    Object resultCcm = exprCcm.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesCcm = (NodeList) resultCcm;
	    
	    Object resultFuel = exprFuel.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesFuel = (NodeList) resultFuel;
	    
	    Object resultMakeNum = exprMakeNum.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesMakeNum = (NodeList) resultMakeNum;
	    
	    Object resultOnSync = exprOnSync.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesOnSync = (NodeList) resultOnSync;
	    
	    Object resultGramsPerKm = exprGramsPerKm.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesGramsPerKm = (NodeList) resultGramsPerKm;
	    
        setting.setManufacturer(nodesManufacturer.item(0).getNodeValue());
        setting.setUserId(Integer.parseInt(nodesUserId.item(0).getNodeValue()));
        setting.setModelName(nodesModelName.item(0).getNodeValue());
        setting.setCcm(nodesCcm.item(0).getNodeValue());
        setting.setFuel(nodesFuel.item(0).getNodeValue());
        setting.setMakeNum(nodesMakeNum.item(0).getNodeValue());
        
        Node n = nodesOnSync.item(0);
        String a = n.getNodeValue();
        
        setting.setSyncOn(Integer.parseInt(nodesOnSync.item(0).getNodeValue()));
        setting.setId(0);
        setting.setGramsPerKm(Double.parseDouble(nodesGramsPerKm.item(0).getNodeValue()));

		
		return setting;
	}
	
	public void addSettings(UserSettings us){
		addSettings(us.getManufacturer(), us.getModelName(), us.getCcm(), us.getFuel(), us.getModelName(), us.getSyncOn(), us.getGramsPerKm());
	}
	
	public void addSettings(String manufacturer, String modelName, 
			String ccm, String fuel, String makeNum, int onSync, double gramPerKm){
		
		initDoc();
		
		NodeList nl = doc.getElementsByTagName("Settings");
        Node b13Node = (Node) nl.item(0);
        
        //settings instance
		Element settings = doc.createElement("Setting");
		b13Node.appendChild(settings);
		
		//manufacturer
 		Element manufacturerNode = doc.createElement("manufacturer");
 		manufacturerNode.appendChild(doc.createTextNode(manufacturer)); // se ni getterjev za idUsers
 		settings.appendChild(manufacturerNode);
 		
		//user id
 		Element userIdNode = doc.createElement("userId");
 		userIdNode.appendChild(doc.createTextNode(Integer.toString(Constants.user.getUserId()))); 
 		settings.appendChild(userIdNode);
 		
 		//model name
 		Element modelNameNode = doc.createElement("modelName");
 		modelNameNode.appendChild(doc.createTextNode(modelName)); 
 		settings.appendChild(modelNameNode);
        
 		//ccm
 		Element ccmNode = doc.createElement("ccm");
 		ccmNode.appendChild(doc.createTextNode(ccm)); 
 		settings.appendChild(ccmNode);
 		
 		//fuel
 		Element fuelNode = doc.createElement("fuel");
 		fuelNode.appendChild(doc.createTextNode(fuel)); 
 		settings.appendChild(fuelNode);
 		
 		//makeNum
 		Element makeNumNode = doc.createElement("makeNum");
 		makeNumNode.appendChild(doc.createTextNode(makeNum)); 
 		settings.appendChild(makeNumNode);
 		
 		//onSnyc
 		Element onSnycNode = doc.createElement("onSync");
 		onSnycNode.appendChild(doc.createTextNode(Integer.toString(onSync))); 
 		settings.appendChild(onSnycNode);
 		
 		//onSnyc
 		Element gramPerKmNode = doc.createElement("gramsPerKm");
 		gramPerKmNode.appendChild(doc.createTextNode(Double.toString(gramPerKm))); 
 		settings.appendChild(gramPerKmNode);
 		
		saveToXml();
	}
	
	public void addCarbonData(String emitterName, int gramsCarbon, int Road_idRoad, int Users_idUsers) throws Exception {
		
		initDoc();
         
        NodeList nl = doc.getElementsByTagName("carbonData");
        Node b13Node = (Node) nl.item(0);
        
        //carbonDataInstance
		Element carbonDataInstance = doc.createElement("carbonDataInstance");
		b13Node.appendChild(carbonDataInstance);

		// idCarbonData
		Element idCarbonDataE = doc.createElement("idCarbonData");
		idCarbonDataE.appendChild(doc.createTextNode(emitterName)); // se ni getterjev za idUsers
		carbonDataInstance.appendChild(idCarbonDataE);
		
		// emitterName
		Element emitterNameE = doc.createElement("emitterName");
		emitterNameE.appendChild(doc.createTextNode(Integer.toString(autoIncrement("carbonData", "carbonDataInstance", "idCarbonData")))); // se ni getterjev za idUsers
		carbonDataInstance.appendChild(emitterNameE);
		
		// gramsCarbon
		Element gramsCarbonE = doc.createElement("gramsCarbon");
		gramsCarbonE.appendChild(doc.createTextNode(Integer.toString(gramsCarbon))); // se ni getterjev za idUsers
		carbonDataInstance.appendChild(gramsCarbonE);
		
		Element Road_idRoadE = doc.createElement("Road_idRoad");
		Road_idRoadE.appendChild(doc.createTextNode(Integer.toString(Road_idRoad))); // se ni getterjev za idUsers
		carbonDataInstance.appendChild(Road_idRoadE);
		
		// Users_idUsers
		Element Users_idUsersE = doc.createElement("Users_idUsers");
		Users_idUsersE.appendChild(doc.createTextNode(Integer.toString(Users_idUsers))); // se ni getterjev za idUsers
		carbonDataInstance.appendChild(Users_idUsersE);

        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer t = tf.newTransformer();
        t.transform(new DOMSource(doc), new StreamResult(System.out));
        
        saveToXml();
	}
	
	public int addRoad(int Users_idUsers_r, double length, int TravelMode_idTravelMode, String DateTime, int travelTime) throws Exception {
		
		int idRoad = autoIncrement("Road", "RoadInstance", "idRoad");

		initDoc();

		NodeList nl = doc.getElementsByTagName("Road");
        Node b13Node = nl.item(0);
        
      //User
		Element RoadInstance = doc.createElement("RoadInstance");
		b13Node.appendChild(RoadInstance);

		// idRoad
		Element idRoadeE = doc.createElement("idRoad");
		idRoadeE.appendChild(doc.createTextNode(Integer.toString(idRoad))); // se ni getterjev za idUsers
		RoadInstance.appendChild(idRoadeE);
		
		// Users_idUsers_r
		Element Users_idUsers_rE = doc.createElement("Users_idUsers_r");
		Users_idUsers_rE.appendChild(doc.createTextNode(Integer.toString(Users_idUsers_r))); // se ni getterjev za idUsers
		RoadInstance.appendChild(Users_idUsers_rE);
 
		// length
		Element lengthE = doc.createElement("length");
		lengthE.appendChild(doc.createTextNode(Double.toString(length))); // se ni getterjev za idUsers
		RoadInstance.appendChild(lengthE);
		
		// TravelMode_idTravelMode
		Element TravelMode_idTravelModeE = doc.createElement("TravelMode_idTravelMode");
		TravelMode_idTravelModeE.appendChild(doc.createTextNode(Integer.toString(TravelMode_idTravelMode))); // se ni getterjev za idUsers
		RoadInstance.appendChild(TravelMode_idTravelModeE);
		
		// DateTime
		Element DateTimeE = doc.createElement("DateTime");
		DateTimeE.appendChild(doc.createTextNode(DateTime)); // se ni getterjev za idUsers
		RoadInstance.appendChild(DateTimeE);
		
		// traveltime
		Element TravelTimeE = doc.createElement("travelTime");
		TravelTimeE.appendChild(doc.createTextNode(Integer.toString(travelTime))); // se ni getterjev za idUsers
		RoadInstance.appendChild(TravelTimeE);

        saveToXml();
		
		return idRoad;
	}
	
	public int addMap(String name, double speed, int Road_idRoad, double latitude, double longitude, int TravelMode_idTravelMode_m) throws Exception {
		
		int idMapPoints = autoIncrement("MapPoints", "MapPoint", "idMapPoints"); 
		
		initDoc();
        
		NodeList nl = doc.getElementsByTagName("MapPoints");
        Node b13Node = nl.item(0);
        
        //User
		Element MapPoint = doc.createElement("MapPoint");
		b13Node.appendChild(MapPoint);

		// idMapPoints
		Element idMapPointsE = doc.createElement("idMapPoints");
		idMapPointsE.appendChild(doc.createTextNode(Integer.toString(idMapPoints))); // se ni getterjev za idUsers
		MapPoint.appendChild(idMapPointsE);
		
		// name
		Element nameE = doc.createElement("name");
		nameE.appendChild(doc.createTextNode(name)); // se ni getterjev za idUsers
		MapPoint.appendChild(nameE);
		
		//speed
		Element speedE = doc.createElement("speed");
		speedE.appendChild(doc.createTextNode(Double.toString(speed))); // se ni getterjev za idUsers
		MapPoint.appendChild(speedE);
 
		// Road_idRoad
		Element Road_idRoadE = doc.createElement("Road_idRoad");
		Road_idRoadE.appendChild(doc.createTextNode(Integer.toString(Road_idRoad))); // se ni getterjev za idUsers
		MapPoint.appendChild(Road_idRoadE);
		
		// latitude
		Element latitudeE = doc.createElement("latitude");
		latitudeE.appendChild(doc.createTextNode(Double.toString(latitude))); // se ni getterjev za idUsers
		MapPoint.appendChild(latitudeE);
		
		// longitude
		Element longitudeE = doc.createElement("longitude");
		longitudeE.appendChild(doc.createTextNode(Double.toString(longitude))); // se ni getterjev za idUsers
		MapPoint.appendChild(longitudeE);
		
		// TravelMode_idTravelMode_m
		Element TravelMode_idTravelMode_mE = doc.createElement("TravelMode_idTravelMode_m");
		TravelMode_idTravelMode_mE.appendChild(doc.createTextNode(Integer.toString(TravelMode_idTravelMode_m))); // se ni getterjev za idUsers
		MapPoint.appendChild(TravelMode_idTravelMode_mE);

        saveToXml();
		
		return idMapPoints;
	}
	
	
	public User selectUser(int id) throws ParserConfigurationException, SAXException, 
    IOException, XPathExpressionException {
		
		initDoc();

	    XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    XPathExpression expr = xpath.compile("//Users/User[idUsers='" +id+"']/idUsers/text()");
	    XPathExpression username = xpath.compile("//Users/User[idUsers='" +id+"']/username/text()");
	    XPathExpression password = xpath.compile("//Users/User[idUsers='" +id+"']/password/text()"); 

	    Object result = expr.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodes = (NodeList) result;
	    
	    Object resultUsername = username.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesUsername = (NodeList) resultUsername;
	    
	    Object resultPassword = password.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesPassword = (NodeList) resultPassword;
	    
	    User user = new User();
	    user.setUserId(Integer.parseInt(nodes.item(0).getNodeValue()));
	    user.setUsername(nodesUsername.item(0).getNodeValue());
	    user.setPassword(nodesPassword.item(0).getNodeValue());
	    	    
	    return user;
	}
	
	public LinkedList<User> selectAllUsers() throws ParserConfigurationException, SAXException, 
    IOException, XPathExpressionException {
		LinkedList<User> results = new LinkedList<User>();
		
		initDoc();

	    XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    XPathExpression expr = xpath.compile("//Users/User/idUsers/text()");
	    XPathExpression username = xpath.compile("//Users/User/username/text()");
	    XPathExpression password = xpath.compile("//Users/User/password/text()");
	    
	    Object result = expr.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodes = (NodeList) result;
	    
	    Object resultUsername = username.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesUsername = (NodeList) resultUsername;
	    
	    Object resultPassword = password.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesPassword = (NodeList) resultPassword;

	    for (int i = 0; i < nodes.getLength(); i++) {
		    User user = new User();
		    user.setUserId(Integer.parseInt(nodes.item(i).getNodeValue()));
		    user.setUsername(nodesUsername.item(i).getNodeValue());
		    user.setPassword(nodesPassword.item(i).getNodeValue());
		    results.add(user);
	    }
	    
	    return results;
		
	}
	
	public Road selectRoad(int id) throws ParserConfigurationException, SAXException, 
    IOException, XPathExpressionException {
		
		initDoc();

	    XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    XPathExpression exprId = xpath.compile("//Road/RoadInstance[idRoad='" +id+"']/idRoad/text()");
	    XPathExpression exprFkUser = xpath.compile("//Road/RoadInstance[idRoad='" +id+"']/Users_idUsers_r/text()");
	    XPathExpression exprLength = xpath.compile("//Road/RoadInstance[idRoad='" +id+"']/length/text()");
	    XPathExpression exprTravelMode = xpath.compile("//Road/RoadInstance[idRoad='" +id+"']/TravelMode_idTravelMode/text()");
	    XPathExpression exprDateTime = xpath.compile("//Road/RoadInstance[idRoad='" +id+"']/DateTime/text()");
	    XPathExpression exprTravelTime = xpath.compile("//Road/RoadInstance[idRoad='" +id+"']/TravelTime/text()");

	    Object resultId = exprId.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesId = (NodeList) resultId;
	    
	    Object resultFkUser = exprFkUser.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesFkUser = (NodeList) resultFkUser;
	    
	    Object resultLength = exprLength.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesLength = (NodeList) resultLength;
	    
	    Object resultTravelMode = exprTravelMode.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesTravelMode = (NodeList) resultTravelMode;
	    
	    Object resultDateTime = exprDateTime.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesDateTime = (NodeList) resultDateTime;
	    
	    Object resultTravelTime = exprTravelTime.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesTravelTime = (NodeList) resultTravelTime;
	    
	    for (int i = 0; i < nodesId.getLength(); i++) {
	        System.out.println(nodesId.item(i).getNodeValue());
     
	    }
	    
	    Road road = new Road();
	    road.setId(Integer.parseInt(nodesId.item(0).getNodeValue()));
	    road.setFkUserId(Integer.parseInt(nodesFkUser.item(0).getNodeValue()));
	    road.setLengthMeters(Integer.parseInt(nodesLength.item(0).getNodeValue()));
	    road.setMode(Integer.parseInt(nodesTravelMode.item(0).getNodeValue()));
	    road.setSqlDate(java.sql.Timestamp.valueOf(nodesDateTime.item(0).getNodeValue()));
	    road.setTravelTime(Integer.parseInt(nodesTravelTime.item(0).getNodeValue()));
	    
	    return road;
		
	}
	
	public LinkedList<Road> selectRoad(User user) throws ParserConfigurationException, SAXException, 
    IOException, XPathExpressionException {
		
		LinkedList<Road> results = new LinkedList<Road>();
		
		initDoc();

	    XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    XPathExpression exprId = xpath.compile("//Road/RoadInstance[Users_idUsers_r='" +user.getUserId()+"']/idRoad/text()");
	    XPathExpression exprFkUser = xpath.compile("//Road/RoadInstance[Users_idUsers_r='" +user.getUserId()+"']/Users_idUsers_r/text()");
	    XPathExpression exprLength = xpath.compile("//Road/RoadInstance[Users_idUsers_r='" +user.getUserId()+"']/length/text()");
	    XPathExpression exprTravelMode = xpath.compile("//Road/RoadInstance[Users_idUsers_r='" +user.getUserId()+"']/TravelMode_idTravelMode/text()");
	    XPathExpression exprDateTime = xpath.compile("//Road/RoadInstance[Users_idUsers_r='" +user.getUserId()+"']/DateTime/text()");
	    XPathExpression exprTravelTime = xpath.compile("//Road/RoadInstance[Users_idUsers_r='" +user.getUserId()+"']/TravelTime/text()");

	    Object resultId = exprId.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesId = (NodeList) resultId;
	    
	    Object resultFkUser = exprFkUser.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesFkUser = (NodeList) resultFkUser;
	    
	    Object resultLength = exprLength.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesLength = (NodeList) resultLength;
	    
	    Object resultTravelMode = exprTravelMode.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesTravelMode = (NodeList) resultTravelMode;
	    
	    Object resultDateTime = exprDateTime.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesDateTime = (NodeList) resultDateTime;
	    
	    Object resultTravelTime = exprTravelTime.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesTravelTime = (NodeList) resultTravelTime;
	    
	    for (int i = 0; i < nodesId.getLength(); i++) {
	        System.out.println(nodesId.item(i).getNodeValue());
     
	    }
	    
	    for (int i = 0; i < nodesId.getLength(); i++) {
	    	Road road = new Road();
	    	road.setId(Integer.parseInt(nodesId.item(i).getNodeValue()));
	 	    road.setFkUserId(Integer.parseInt(nodesFkUser.item(i).getNodeValue()));
	 	    road.setLengthMeters(Integer.parseInt(nodesLength.item(i).getNodeValue()));
	 	    road.setMode(Integer.parseInt(nodesTravelMode.item(i).getNodeValue()));
	 	    road.setSqlDate(java.sql.Timestamp.valueOf(nodesDateTime.item(i).getNodeValue()));
	 	    road.setTravelTime(Integer.parseInt(nodesTravelTime.item(i).getNodeValue()));
	 	    results.add(road);
	    }

	    return results;
		
	}
	
	public LinkedList<Road> SelectAllRoads()  throws ParserConfigurationException, SAXException, 
    IOException, XPathExpressionException{
		LinkedList<Road> results = new LinkedList<Road>();
		
		initDoc();

	    XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    
	   
	    XPathExpression exprId = xpath.compile("//Road/RoadInstance/idRoad/text()");
	    XPathExpression exprFkUser = xpath.compile("//Road/RoadInstance/Users_idUsers_r/text()");
	    XPathExpression exprLength = xpath.compile("//Road/RoadInstance/length/text()");
	    XPathExpression exprTravelMode = xpath.compile("//Road/RoadInstance/TravelMode_idTravelMode/text()");
	    XPathExpression exprDateTime = xpath.compile("//Road/RoadInstance/DateTime/text()");
	    XPathExpression exprTravelTime = xpath.compile("//Road/RoadInstance/travelTime/text()");

		Object resultId = exprId.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesId = (NodeList) resultId;
	    
	    Object resultFkUser = exprFkUser.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesFkUser = (NodeList) resultFkUser;
	    
	    Object resultLength = exprLength.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesLength = (NodeList) resultLength;
	    
	    Object resultTravelMode = exprTravelMode.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesTravelMode = (NodeList) resultTravelMode;
	    
	    Object resultDateTime = exprDateTime.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesDateTime = (NodeList) resultDateTime;
	    
	    Object resultTravelTime = exprTravelTime.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesTravelTime = (NodeList) resultTravelTime;
	    
	    for (int i = 0; i < nodesId.getLength(); i++) {
	    	Road road = new Road();
	    	road.setId(Integer.parseInt(nodesId.item(i).getNodeValue()));
	 	    road.setFkUserId(Integer.parseInt(nodesFkUser.item(i).getNodeValue()));
	 	    road.setLengthMeters(Double.parseDouble(nodesLength.item(i).getNodeValue()));
	 	    road.setMode(Integer.parseInt(nodesTravelMode.item(i).getNodeValue()));
	 	    road.setSqlDate(java.sql.Timestamp.valueOf(nodesDateTime.item(i).getNodeValue()));
	 	    road.setTravelTime(Integer.parseInt(nodesTravelTime.item(i).getNodeValue()));
	 	    results.add(road);
	    }
	    
		return results;
	}
	
	public CarbonData selectCarbonDataId(int id) throws ParserConfigurationException, SAXException, 
    IOException, XPathExpressionException {
		
		initDoc();

	    XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    XPathExpression idCarbonData = xpath.compile("//carbonData/carbonDataInstance[idCarbonData='" +id+"']/idCarbonData/text()");
	    XPathExpression emitterName = xpath.compile("//carbonData/carbonDataInstance[idCarbonData='" +id+"']/emitterName/text()");
	    XPathExpression gramsCarbon = xpath.compile("//carbonData/carbonDataInstance[idCarbonData='" +id+"']/gramsCarbon/text()");
	    XPathExpression Road_idRoad = xpath.compile("//carbonData/carbonDataInstance[idCarbonData='" +id+"']/Road_idRoad/text()");
	    XPathExpression Users_idUsers = xpath.compile("//carbonData/carbonDataInstance[idCarbonData='" +id+"']/Users_idUsers/text()");
	    
	    Object idCarbonDataResult = idCarbonData.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesCarbonDataResult = (NodeList) idCarbonDataResult;
	    
	    Object emitterNameResult = emitterName.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesemitterNameResult = (NodeList) emitterNameResult;	    
	    
	    Object gramsCarbonResult = gramsCarbon.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesgramsCarbonResult = (NodeList) gramsCarbonResult;
	    
	    Object Road_idRoadResult = Road_idRoad.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesRoad_idRoadResult = (NodeList) Road_idRoadResult;
	    
	    Object Users_idUsersResult = Users_idUsers.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesUsers_idUsersResult = (NodeList) Users_idUsersResult;
	    
	    for (int i = 0; i < nodesCarbonDataResult.getLength(); i++) {
	        System.out.println(nodesCarbonDataResult.item(i).getNodeValue());
	        System.out.println(nodesemitterNameResult.item(i).getNodeValue());
	        System.out.println(nodesgramsCarbonResult.item(i).getNodeValue());
	        System.out.println(nodesUsers_idUsersResult.item(i).getNodeValue());
	        
	    }
	    

	    CarbonData carbonData = new CarbonData();
	    carbonData.setCarbonDataId(Integer.parseInt(nodesCarbonDataResult.item(0).getNodeValue()));
	    carbonData.setEmitterName((nodesemitterNameResult.item(0).getNodeValue()));
	    carbonData.setGramsCarbon(Integer.parseInt(nodesgramsCarbonResult.item(0).getNodeValue()));
	    carbonData.setFkRoadId(Integer.parseInt(nodesRoad_idRoadResult.item(0).getNodeValue()));
	    carbonData.setFkUser(Integer.parseInt(nodesUsers_idUsersResult.item(0).getNodeValue()));
	    
	   return carbonData;
	}
	
	public CarbonData selectCarbonDataUser(int Users_idUsers) throws ParserConfigurationException, SAXException, 
    IOException, XPathExpressionException {
		
		initDoc();

	    XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    XPathExpression idCarbonData = xpath.compile("//carbonData/carbonDataInstance[Users_idUsers='" +Users_idUsers+"']/idCarbonData/text()");
	    XPathExpression emitterName = xpath.compile("//carbonData/carbonDataInstance[Users_idUsers='" +Users_idUsers+"']/emitterName/text()");
	    XPathExpression gramsCarbon = xpath.compile("//carbonData/carbonDataInstance[Users_idUsers='" +Users_idUsers+"']/gramsCarbon/text()");
	    XPathExpression Road_idRoad = xpath.compile("//carbonData/carbonDataInstance[Users_idUsers='" +Users_idUsers+"']/Road_idRoad/text()");
	    XPathExpression Users_idUsersx = xpath.compile("//carbonData/carbonDataInstance[Users_idUsers='" +Users_idUsers+"']/Users_idUsers/text()");

	    Object idCarbonDataResult = idCarbonData.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesCarbonDataResult = (NodeList) idCarbonDataResult;
	    
	    Object emitterNameResult = emitterName.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesemitterNameResult = (NodeList) emitterNameResult;	    
	    
	    Object gramsCarbonResult = gramsCarbon.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesgramsCarbonResult = (NodeList) gramsCarbonResult;
	    
	    Object Road_idRoadResult = Road_idRoad.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesRoad_idRoadResult = (NodeList) Road_idRoadResult;
	    
	    Object Users_idUsersResult = Users_idUsersx.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesUsers_idUsersResult = (NodeList) Users_idUsersResult;
	    
	    for (int i = 0; i < nodesCarbonDataResult.getLength(); i++) {
	        System.out.println(nodesCarbonDataResult.item(i).getNodeValue());
	        System.out.println(nodesemitterNameResult.item(i).getNodeValue());
	        System.out.println(nodesgramsCarbonResult.item(i).getNodeValue());
	        System.out.println(nodesUsers_idUsersResult.item(i).getNodeValue());
	        
	    }
	    
	    CarbonData carbonData = new CarbonData();
	    carbonData.setCarbonDataId(Integer.parseInt(nodesCarbonDataResult.item(0).getNodeValue()));
	    carbonData.setEmitterName((nodesemitterNameResult.item(0).getNodeValue()));
	    carbonData.setGramsCarbon(Integer.parseInt(nodesgramsCarbonResult.item(0).getNodeValue()));
	    carbonData.setFkRoadId(Integer.parseInt(nodesRoad_idRoadResult.item(0).getNodeValue()));
	    carbonData.setFkUser(Integer.parseInt(nodesUsers_idUsersResult.item(0).getNodeValue()));
	    
	    return carbonData;
	}
	
	public LinkedList<MapPoint> selectAllMapPoints() throws ParserConfigurationException, SAXException, 
    IOException, XPathExpressionException{
		LinkedList<MapPoint> results = new LinkedList<MapPoint>();
		
		initDoc();

	    XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    
	   
	    XPathExpression exprId = xpath.compile("//MapPoints/MapPoint/idMapPoints/text()");
	    XPathExpression exprName = xpath.compile("//MapPoints/MapPoint/name/text()");
	    XPathExpression exprSpeed = xpath.compile("//MapPoints/MapPoint/speed/text()");
	    XPathExpression exprRoad_idRoad = xpath.compile("//MapPoints/MapPoint/Road_idRoad/text()");
	    XPathExpression exprLatitude = xpath.compile("//MapPoints/MapPoint/latitude/text()");
	    XPathExpression exprLongitude = xpath.compile("//MapPoints/MapPoint/longitude/text()");
	    XPathExpression exprTravelMode = xpath.compile("//MapPoints/MapPoint/TravelMode_idTravelMode_m/text()");
	    
		Object resultId = exprId.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesId = (NodeList) resultId;
	    
	    Object resultName = exprName.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesName = (NodeList) resultName;
	    
	    Object resultSpeed = exprSpeed.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesSpeed = (NodeList) resultSpeed;
	    
	    Object resultTravelMode = exprTravelMode.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesTravelMode = (NodeList) resultTravelMode;
	    
	    Object resultRoad_idRoad = exprRoad_idRoad.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesRoad_idRoad = (NodeList) resultRoad_idRoad;
	    
	    Object resultLatitude = exprLatitude.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesLatitude = (NodeList) resultLatitude;
	    
	    Object resultLongitude = exprLongitude.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesLongitude = (NodeList) resultLongitude;
	    
	    for (int i = 0; i < nodesId.getLength(); i++) {
	    	MapPoint mapPoint = new MapPoint(0,0);
	    	
	    	mapPoint.id = Integer.parseInt(nodesId.item(i).getNodeValue());
	    	mapPoint.mName = nodesName.item(i).getNodeValue();
	    	mapPoint.mSpeed = Double.parseDouble(nodesSpeed.item(i).getNodeValue());
	    	mapPoint.mTravelMode = Integer.parseInt(nodesTravelMode.item(i).getNodeValue());
	    	mapPoint.fkIdRoad = Integer.parseInt(nodesRoad_idRoad.item(i).getNodeValue());
	    	mapPoint.mLatitude = Double.parseDouble(nodesLatitude.item(i).getNodeValue());
	    	mapPoint.mLongitude = Double.parseDouble(nodesLongitude.item(i).getNodeValue());
	    	
	 	    results.add(mapPoint);
	    }
	    
		return results;
	}
	
	public LinkedList<MapPoint> selectMapPoints(int Road_idRoad) throws ParserConfigurationException, SAXException, 
    IOException, XPathExpressionException{
		LinkedList<MapPoint> results = new LinkedList<MapPoint>();
		
		initDoc();

	    XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    
	   
	    XPathExpression exprId = xpath.compile("//MapPoints/MapPoint[Road_idRoad='"+Road_idRoad+"']/idMapPoints/text()");
	    XPathExpression exprName = xpath.compile("//MapPoints/MapPoint[Road_idRoad='"+Road_idRoad+"']/name/text()");
	    XPathExpression exprSpeed = xpath.compile("//MapPoints/MapPoint[Road_idRoad='"+Road_idRoad+"']/speed/text()");
	    XPathExpression exprRoad_idRoad = xpath.compile("//MapPoints/MapPoint[Road_idRoad='"+Road_idRoad+"']/Road_idRoad/text()");
	    XPathExpression exprLatitude = xpath.compile("//MapPoints/MapPoint[Road_idRoad='"+Road_idRoad+"']/latitude/text()");
	    XPathExpression exprLongitude = xpath.compile("//MapPoints/MapPoint[Road_idRoad='"+Road_idRoad+"']/longitude/text()");
	    XPathExpression exprTravelMode = xpath.compile("//MapPoints/MapPoint[Road_idRoad='"+Road_idRoad+"']/TravelMode_idTravelMode_m/text()");
	    
		Object resultId = exprId.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesId = (NodeList) resultId;
	    
	    Object resultName = exprName.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesName = (NodeList) resultName;
	    
	    Object resultSpeed = exprSpeed.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesSpeed = (NodeList) resultSpeed;
	    
	    Object resultTravelMode = exprTravelMode.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesTravelMode = (NodeList) resultTravelMode;
	    
	    Object resultRoad_idRoad = exprRoad_idRoad.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesRoad_idRoad = (NodeList) resultRoad_idRoad;
	    
	    Object resultLatitude = exprLatitude.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesLatitude = (NodeList) resultLatitude;
	    
	    Object resultLongitude = exprLongitude.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesLongitude = (NodeList) resultLongitude;
	    
	    for (int i = 0; i < nodesId.getLength(); i++) {
	    	MapPoint mapPoint = new MapPoint(0,0);
	    	
	    	mapPoint.id = Integer.parseInt(nodesId.item(i).getNodeValue());
	    	mapPoint.mName = nodesName.item(i).getNodeValue();
	    	mapPoint.mSpeed = Double.parseDouble(nodesSpeed.item(i).getNodeValue());
	    	mapPoint.mTravelMode = Integer.parseInt(nodesTravelMode.item(i).getNodeValue());
	    	mapPoint.fkIdRoad = Integer.parseInt(nodesRoad_idRoad.item(i).getNodeValue());
	    	mapPoint.mLatitude = Double.parseDouble(nodesLatitude.item(i).getNodeValue());
	    	mapPoint.mLongitude = Double.parseDouble(nodesLongitude.item(i).getNodeValue());
	    	
	 	    results.add(mapPoint);
	    }
	    
		return results;
	}
	
	public void updateUser(User user) throws ParserConfigurationException, SAXException, 
    IOException, XPathExpressionException, TransformerException {
		
		int id = user.getUserId();
		String password = user.getPassword();
		String username = user.getUsername();
		
		initDoc();

	    XPathFactory factory2 = XPathFactory.newInstance();
	    XPath xpath = factory2.newXPath();
	    XPathExpression exprId = xpath.compile("//Users/User[idUsers='" +id+"']/idUsers/text()");
	    XPathExpression exprUsername = xpath.compile("//Users/User[idUsers='" +id+"']/username/text()");
	    XPathExpression exprPassword = xpath.compile("//Users/User[idUsers='" +id+"']/password/text()");
	    
	    Object resultId = exprId.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesId = (NodeList) resultId;
	    
	    Object resultUsername = exprUsername.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesUsername = (NodeList) resultUsername;
	    
	    Object resultPassword = exprPassword.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesPassword = (NodeList) resultPassword;
	    
	    nodesId.item(0).setNodeValue(Integer.toString(id));
	    nodesUsername.item(0).setNodeValue(username);
	    nodesPassword.item(0).setNodeValue(password);
	    //nodes.item(4).setNodeValue("mamba");
		
	    saveToXml();
	  
	}
	
	public void updateRoad(Road road) throws ParserConfigurationException, SAXException, 
    IOException, XPathExpressionException, TransformerException {
		
		int id = road.getId();
		int id_User = road.getFkUserId();
		double length = road.getLengthMeters();
		int travelMode = road.getMode();
		int travelTime = road.getTravelTime();
		java.sql.Timestamp sqlDate = road.getSqlDate();
		
		initDoc();

	    XPathFactory factory2 = XPathFactory.newInstance();
	    XPath xpath = factory2.newXPath();
	    XPathExpression exprId = xpath.compile("//Road/RoadInstance[idRoad='" +id+"']/idRoad/text()");
	    XPathExpression exprFkUser = xpath.compile("//Road/RoadInstance[idRoad='" +id+"']/Users_idUsers_r/text()");
	    XPathExpression exprLength = xpath.compile("//Road/RoadInstance[idRoad='" +id+"']/length/text()");
	    XPathExpression exprTravelMode = xpath.compile("//Road/RoadInstance[idRoad='" +id+"']/TravelMode_idTravelMode/text()");
	    XPathExpression exprDateTime = xpath.compile("//Road/RoadInstance[idRoad='" +id+"']/DateTime/text()");
	    XPathExpression exprTravelTime = xpath.compile("//Road/RoadInstance[idRoad='" +id+"']/TravelTime/text()");
	    
	    
	    
	    Object resultId = exprId.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesId = (NodeList) resultId;
	    
	    Object resultFkUser = exprFkUser.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesFkUser = (NodeList) resultFkUser;
	    
	    Object resultLength = exprLength.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesLength = (NodeList) resultLength;
	    
	    Object resultTravelMode = exprTravelMode.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesTravelMode = (NodeList) resultTravelMode;
	    
	    Object resultDateTime = exprDateTime.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesDateTime = (NodeList) resultDateTime;
	    
	    Object resultTravelTime = exprTravelTime.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesTravelTime = (NodeList) resultTravelTime;
	    
	    nodesId.item(0).setNodeValue(Integer.toString(id));
	    nodesFkUser.item(0).setNodeValue(Integer.toString(id_User));
	    nodesLength.item(0).setNodeValue(Double.toString(length));
	    nodesTravelMode.item(0).setNodeValue(Integer.toString(travelMode));
	    nodesDateTime.item(0).setNodeValue(sqlDate.toString());
	    nodesTravelTime.item(0).setNodeValue(Integer.toString(travelTime));
	    
	    saveToXml();
	}
	
	public void saveToXml(){
		try{
			OutputStream os = con.openFileOutput("baza.xml", Context.MODE_PRIVATE);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
			
			TransformerFactory factory = TransformerFactory.newInstance(); 
			Transformer transformer = factory.newTransformer(); 
	
			transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
	
			StringWriter sw = new StringWriter(); 
			StreamResult result = new StreamResult(sw); 
			DOMSource source = new DOMSource(doc); 
			transformer.transform(source, result); 
			String xmlString = sw.toString(); 
			bw.write(xmlString); 
			bw.flush(); 
			bw.close();
		}catch(Exception e){
			e.printStackTrace();
		}
			
	}
	
	public void deleteUser(User user) {
		try {
			deleteUser(user.getUserId());
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public void deleteUser(int id) throws Exception {
		
		initDoc();
		
        NodeList nl = doc.getElementsByTagName("Users");
        Node b13Node = nl.item(0);
        
        b13Node.removeChild(b13Node);

        saveToXml();
	}
	
	public void deleteRoad(int id) throws Exception {
		initDoc();

		NodeList nl = doc.getElementsByTagName("Road");
        Node b13Node = nl.item(0);
        b13Node.removeChild(b13Node);

        saveToXml();
	}
	
	public void deleteMapPoints(int id) throws Exception {
		initDoc();
		
		NodeList nl = doc.getElementsByTagName("MapPoints");

        Node b13Node = nl.item(0);
        b13Node.removeChild(b13Node);

        saveToXml();
	}
	
	public void deleteCarbonData(int id) throws Exception {
		initDoc();
		
		NodeList nl = doc.getElementsByTagName("carbonData");

        Node b13Node = nl.item(0);
        b13Node.removeChild(b13Node);

        saveToXml();
	}
	
	
	public void addUser(User user){
		
		try{
			initDoc();
		
			String usernameX = user.getUsername();
			String passwordX = user.getPassword();
			String id = Integer.toString(autoIncrement("Users", "User", "idUsers"));
				        
	        XPathFactory xpf = XPathFactory.newInstance();
	        XPath xpath = xpf.newXPath();
	        XPathExpression expression = xpath.compile("//baza/Users/text()");
	
	        NodeList nl = (NodeList) doc.getElementsByTagName("Users");
	        Node usersNode = nl.item(0);
	        //User
			Element User = doc.createElement("User");
			usersNode.appendChild(User);
	
			// idUsers
			Element idUsers = doc.createElement("idUsers");
			idUsers.appendChild(doc.createTextNode(id)); // se ni getterjev za idUsers
			User.appendChild(idUsers);
	 
			// username
			Element username = doc.createElement("username");
			username.appendChild(doc.createTextNode(usernameX)); // se ni getterjev za username
			User.appendChild(username);
	 
			// password
			Element password = doc.createElement("password");
			password.appendChild(doc.createTextNode(passwordX)); // se ni getterjev za password
			User.appendChild(password);
	 
			saveToXml();
		
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

	
	public int autoIncrement(String root, String object, String idString) throws XPathExpressionException, SAXException, IOException, ParserConfigurationException {
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
	    domFactory.setNamespaceAware(true); // never forget this!
	    domFactory.setIgnoringElementContentWhitespace(true);
	    DocumentBuilder builder = domFactory.newDocumentBuilder();
	    // Document doc = builder.parse()

	    XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    
	    XPathExpression exprIds = xpath.compile("//"+root+"/"+object+"/"+idString+"/text()");
	    
	    Object ids = exprIds.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodesIds = (NodeList) ids;
	    int newId = 0;
	    
	    for (int j = 0; j < nodesIds.getLength(); j++) {
	    	int contained = 0;
	    	for (int i = 0; i < nodesIds.getLength(); i++) {
	    		int id = Integer.parseInt(nodesIds.item(i).getNodeValue());
	    		if (id != j) {
	    			contained++;
	    		}
	    	}
	    	if (contained == 0) {
	    		return j;
	    	}
	    }
	    
	    return nodesIds.getLength()+1;
	}
	
}
