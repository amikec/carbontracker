package com.carbon.tracker;

import java.io.IOException;import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import com.carbon.tracker.entity.MapPoint;
import com.carbon.tracker.entity.Road;
import com.carbon.tracker.entity.SaveToXML;
import com.carbon.tracker.gps.GPSCallback;
import com.carbon.tracker.gps.GPSManager;
import com.carbon.tracker.map.*;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.MenuItem.OnMenuItemClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class GoogleMapsActivity extends MapActivity implements GPSCallback, LocationListener{
	private boolean onBack_pressed = false; 
	public LinearLayout linearLayout;
    public MapView mapView;
    private int gps=0;
    public static Address selectedAddress;
    public static MapPoint endPoint;
    public static MapPoint startPoint; 
    public Context mainContext = this;
	public static int runMode;
	private TextView textView;
    private List<Overlay> listOfOverlays;
    private MyLocationOverlay myLocationOverlay;
    
    private LinkedList<Road> roads = new LinkedList<Road>();
    public LinkedList<Road> getRoads() {
		return roads;
	}

	public void setRoads(LinkedList<Road> roads) {
		this.roads = roads;
	}

    private GPSManager gpsMng = null; 
    public GPSManager getGpsMng() {
		return gpsMng;
	}

	public void setGpsMng(GPSManager gpsMng) {
		this.gpsMng = gpsMng;
	}

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
            super.onCreate(savedInstanceState);
            setContentView(R.layout.map);
            
            
            
            this.setRoads(new LinkedList<Road>());
            
            /*
             * Objekt v katerega smo shranili podatke activity-ja preden se je le ta restartal
             */
            final ConfigData data = (ConfigData) getLastNonConfigurationInstance();
                        
            if(data != null){
            	
    			gpsMng = data.gpsManager;
    			endPoint = data.endPoint;
    			startPoint = data.startPoint;
    			mapView = data.mapView;
    			linearLayout = data.linearLayout;
    			runMode = data.runMode;
    			listOfOverlays = data.listOfOverlays;
    			selectedAddress = data.selectedAddress;
    			roads = data.roads;
    			textView = data.textView;
    			
    			
            }else{
            	
            	startPoint = null;
            	endPoint = null;
            
                runMode = getIntent().getExtras().getInt("startMode");
                
                textView = (TextView) findViewById(R.id.description);
                
               	mapView = (MapView) findViewById(R.id.mapview);
                mapView.setBuiltInZoomControls(true);
              
                gpsMng = new GPSManager();
                gpsMng.setGPSCallback(this);
                
            }
            
            Constants.runMode = runMode;
            
            /*
             * Overlay, ki skrbi za prikaz trenutne lokacije (utripajoc krogec)
             */
            myLocationOverlay = new MyLocationOverlay(this, mapView);
            myLocationOverlay.enableCompass(); 
            myLocationOverlay.enableMyLocation();
            
            /*
             * Seznam vseh overlayev na trenutni mapi.
             */
            listOfOverlays = mapView.getOverlays();
            listOfOverlays.add(new MapOverlay(this, mapView));
            listOfOverlays.add(myLocationOverlay);
            
            gpsMng.setGPSCallback(this);
            gpsMng.startListening(getApplicationContext());

    		mapView.getController().setZoom(18);
            
            if(data == null && runMode == Constants.START_MODE_TRACK)
                	runTracking();
            
            if(startPoint != null)
            	gpsMng.stopListening();

    }
    public Handler drawHandler = new Handler(){
    	public void handleMessage(Message msg) {
    		mapView.invalidate();
    	};
    };
    
            
    /*
     * Vmesna metoda za risanje poti, ki ji lahko podamo seznam poti za izris.
     */
    private void drawRoute(LinkedList<Road> roadsParam){

    	if(this.roads == null)
    		this.roads = new LinkedList<Road>();
    	
    	for(Road road : roadsParam){
    		this.roads.add(road);
    	}
    	
    	drawRoute();
    }
    
    /*
     * Metoda zazene novo nit, ki poskrbi za izris poti na mapi.
     * Ko klicemo mapView.invalidate(), se sprozi draw() metoda, overrideana v MapOverlay.
     */
    private void drawRoute(){
    	
        Runnable draw = new Runnable(){
        	
			public void run() {
				
				listOfOverlays.clear();
				
				for(int i = 0; i < roads.size(); i++){
					Road road = roads.get(i);
					
					MapOverlay mapOverlay = new MapOverlay(GoogleMapsActivity.this, road, mapView);
					listOfOverlays.add(mapOverlay);
					
				}
				
				beerLocationMarker();
				
				if(!listOfOverlays.contains(myLocationOverlay))
                	listOfOverlays.add(myLocationOverlay);
				
				drawHandler.sendEmptyMessage(0);
			}
			
			private void beerLocationMarker(){
				
				GeoPoint p = new GeoPoint((int)(52.294672220014036*1E6), (int)(4.820721186697483*1E6));
				
				Drawable drawable = GoogleMapsActivity.this.getResources().getDrawable(R.drawable.androidmarker);
		    	ItemizedOverlayImpl itemizedoverlay = new ItemizedOverlayImpl(drawable, GoogleMapsActivity.this);  
		    	OverlayItem overlayitem = new OverlayItem(p, "A sixpack of Heineken", "If you find it, leave a note with your group country name :)");
		    	itemizedoverlay.addOverlay(overlayitem);
		    	mapView.getOverlays().add(itemizedoverlay);
			}
        };
        
        new Thread(draw).start();
    }
    
    @Override
    protected boolean isRouteDisplayed() {
            return false;
    }
	
	/*
	 * Ob unicenju trenutne aktivnosti.
	 */
	@Override
    protected void onDestroy() {
       
		Bundle outState = new Bundle();
		onSaveInstanceState(outState);
		
		gpsMng.stopListening();
        gpsMng.setGPSCallback(null);
        
        super.onDestroy();
    }
	
	@Override
	public void onBackPressed() {
		
		if(Constants.runMode == Constants.START_MODE_TRACK && !onBack_pressed){
			
			
			final CharSequence[] items = {"View this route statistics ", "Exit to main"};
			AlertDialog.Builder builder = new AlertDialog.Builder(GoogleMapsActivity.this);
			builder.setCancelable(true);
			builder.setTitle(R.string.Options);
			builder.setItems(items, new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					//torej ravnaj se po int which.. ce je 0 = statistic, 1=exit
					
					if(which==0){
						endTracking();
						startActivity(new Intent(GoogleMapsActivity.this, SettingsActivity.class));
					}else{
						System.exit(0);
						onBack_pressed = true;						
					}		
				}
			});
			AlertDialog alert=builder.create();
			alert.show();
			}
			
			if(onBack_pressed){
				super.onBackPressed();
					
			}
			onBack_pressed = true;
		
	}
	/*
     * GPS preverjanje
     */
	@Override
	protected void onResume() {
		super.onResume();
		if(gps<2){
	        try {
				// get location manager to check gps availability
	        	LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
				locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000L,1.0f, this);
			
				boolean isGPS = locationManager.isProviderEnabled (LocationManager.GPS_PROVIDER); 
	
				if(!isGPS){
					gps++;
					if(gps<2) checkGps();
					else finish();
				}
				else{
					gps=2;				
					//gps is available, do actions here		    			        
				}				
			
		 	} catch (Exception e1) {
		 		gps++;
		 		if(gps<2) checkGps();
		 		else finish();
			}
        }
		
		
	}
	public void checkGps(){
		final CharSequence[] items = {"Eneble gps ", "Exit"};
		AlertDialog.Builder builder = new AlertDialog.Builder(GoogleMapsActivity.this);
		builder.setCancelable(true);
		builder.setTitle(R.string.No_gps);
		builder.setItems(items, new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				//torej ravnaj se po int which.. ce je 0 = statistic, 1=exit
				
				if(which==0){
					startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
				}else{
					System.exit(0);						
				}		
			}
		});
		AlertDialog alert=builder.create();
		alert.show();
    }
	/*
	 * Preden se trenutna aktivnost unici, shranimo vse podatke.
	 */
	@Override
	public Object onRetainNonConfigurationInstance() {
		
		final ConfigData data = new ConfigData();
		data.gpsManager = this.gpsMng;
		data.endPoint = endPoint;
		data.startPoint = startPoint;
		data.mapView = mapView;
		data.linearLayout = linearLayout;
		data.runMode = runMode;
		data.listOfOverlays = listOfOverlays;
		data.selectedAddress = selectedAddress;
		data.roads = roads;
		data.textView = textView;
		
		if(onBack_pressed == true)
			return null;
		
		return data;
	}
	
	/*
	 *----------------------------------
	 * ------- TRACKING MODE -----------
	 * ---------------------------------
	 */
	
    /*
     * Callback metoda, ki se prozi, ko GPS zazna premik. Uporabljamo jo, ko smo v nacinu "tracking".
     * V globalno spremenljivko pot doda tocko in klice ponovni izris.
     */
	public void onGPSUpdateTracking(Location location) {
		
		if(roads.size() < 1)
			roads.add(new Road());
		
		float speed = (location.getSpeed() * 1000) / 3600;
		int lat = (int) (location.getLatitude() * 1E6);
		int lon = (int) (location.getLongitude() * 1E6);
		
		textView.setText("Coordinates: " + location.getLatitude() + " | " + location.getLongitude() + "\n" + 
						 "Speed: " + speed + " km/h");
		
		Road trackedRoad = this.getRoads().getFirst();
		
		MapPoint newPoint = new MapPoint(lat, lon);
		newPoint.mSpeed = speed;
		newPoint.mTravelMode = trackedRoad.getMode();
		
		trackedRoad.getmRoute().add(newPoint);
		
		if(trackedRoad.getStartTime() == null)
			trackedRoad.setStartTime();
		
		// Focus na točko.
		mapView.getController().animateTo(new GeoPoint(lat, lon));
					
		drawRoute();
		
	}
	
	
	
	/*
	 * RunMode = tracking
	 * Prikaze dialog za izbiro nacina potovanja, v globalni seznam poti doda novo pot,
	 * v katero bomo sproti dodajali nove tocke in jo izrisovali.
	 */
	private void runTracking(){
		final CharSequence[] items = {"Walk", "Bike", "Car", "Bus"};
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(true);
		builder.setOnCancelListener(new OnCancelListener() {
			
			public void onCancel(DialogInterface dialog) {
				if(roads.isEmpty()){
					Road road = new Road();
					road.setMode(2);
					roads.add(road);
				}else{
					roads.getFirst().setMode(2);
					roads.add(roads.getFirst());
				}
			}
		});
		builder.setTitle(R.string.means_transport);
		builder.setItems(items, new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				//torej ravnaj se po int which.. ce je 0 = Walk... 3 = Bus
				
				if(roads.isEmpty()){
					Road road = new Road();
					road.setMode(which);
					roads.add(road);
				}else{
					roads.getFirst().setMode(which);
					roads.add(roads.getFirst());
				}				
			}
		});
		AlertDialog alert=builder.create();
		alert.show();
	}
	
	/*
	 * ----------------------------------------
	 * --------- END TRACKING MODE ------------
	 * -----START DESIRED LOCATION MODE -------
	 * ----------------------------------------
	 */
	
	/*
	 * Callback metoda, ki se prozi, ko GPS zazna premik. Uporabljamo jo, ko smo v načinu "desired location".
	 * Pridobi zacetno lokacijo, nato prikaze dialog za izbiro zeljene destinacije.
	 */
	public void onGPSUpdateDesired(Location location){
		
		if(startPoint != null){
			this.gpsMng.stopListening();
			return;
		}
		
		float speed = (location.getSpeed() * 1000) / 3600;
		int lat = (int) (location.getLatitude() * 1E6);
		int lon = (int) (location.getLongitude() * 1E6);
		textView.setText("Coordinates: " + lat + " | " + lon + "\n" + 
						 "Speed: " + speed + " km/h");
		
		startPoint = new MapPoint(lat, lon);
		startPoint.mSpeed = speed;
		
		this.gpsMng.stopListening();		
		chooseEndLocationDialog();
	}
	
	/*
	 * RunMode = "desiredlocation"
	 * Prikaze dialog za nacin izbire koncne lokacije (klik na mapo ali vpis naslova).
	 */
	public void chooseEndLocationDialog(){
		enterLocation();
		listOfOverlays.clear();
	}
	
    /*
     * Handler, ki zazene novo nit, ki pridobi podatke o moznih poteh ter jih primerja in izrise.
     */
    public Handler handleDialog = new Handler(){
    	public void handleMessage(Message msg) {
    		
    		try {
	    		Object[] params = new Object[]{GoogleMapsActivity.this, GoogleMapsActivity.startPoint, GoogleMapsActivity.endPoint};
	    		
	    		CompareRoutesAsync getRoutes = new CompareRoutesAsync();
	    		getRoutes.execute(params);
    		
				Object[] result = getRoutes.get();
				LinkedList<Road> roadsResult = (LinkedList<Road>)result[0];
				
				drawRoute(roadsResult);
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
    	};
    };
	
	/*
	 * Inicializiramo svoje custom dialoge, s parametrom "id" izberemo katerega vrnemo.
	 */
	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog = new Dialog(this);
	    switch(id) {
	    case 0: //dialog, ki se ukvarja s naslovi
	    	dialog.setContentView(R.layout.custom_dialog);
			dialog.setTitle(R.string.custom_dialog_text);
			EditText textInput = (EditText) dialog.findViewById(R.id.editText_custom);
			ListView addresses = (ListView) dialog.findViewById(R.id.listView_custom);
		 
			textInput.addTextChangedListener(new MyTextWatcher(addresses, dialog) {				
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					
					ConnectivityManager conMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
			    	NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
			    	
			    	if(netInfo !=null && netInfo.isConnected()){
			    		Object [] toAddressTask = {s};
						AddressTask addrTask = new AddressTask();
						addrTask.execute(toAddressTask);
						ArrayAdapter<String> adapter = null;
						
						try {
							final List<Address> addresses = addrTask.get();
							List<String> seznamNaslovov = addressesToString(addresses);
							adapter = new ArrayAdapter<String>(GoogleMapsActivity.this, R.layout.addresses_item, seznamNaslovov);
							
							this.listView.setAdapter(adapter);
							this.listView.setOnItemClickListener(new OnItemClickListener() {

								public void onItemClick(AdapterView<?> arg0,
										View arg1, int arg2, long arg3) {
									//Adapter adapt = arg0.getAdapter();
									Address a = addresses.get(arg2);
									selectedAddress = a;
									endPoint = new MapPoint((int)(a.getLatitude()*1E6), (int)(a.getLongitude()*1E6));
									
									//sporocimo handlerju, da smo izbrali koncno lokacijo
									GoogleMapsActivity.this.handleDialog.sendEmptyMessage(0);
									
									dialog.dismiss();
									
								}
							});
							
						} catch (InterruptedException e) {
							e.printStackTrace();
						} catch (ExecutionException e) {
							e.printStackTrace();
						}
			    	}else{
			    		Toast.makeText(getApplicationContext(), "You have no internet connection", Toast.LENGTH_SHORT).show();
			    		final CharSequence[] items = {"Set my internet settings", "Exit to main"};
			    		AlertDialog.Builder builder = new AlertDialog.Builder(GoogleMapsActivity.this);
			    		builder.setCancelable(true);
			    		builder.setOnCancelListener(new OnCancelListener() {
			    			
			    			public void onCancel(DialogInterface dialog) {
			    				System.exit(0);
			    			}
			    		});
			    		builder.setTitle(R.string.network_settings);
			    		builder.setItems(items, new DialogInterface.OnClickListener() {
			    			
			    			public void onClick(DialogInterface dialog, int which) {
			    				//torej ravnaj se po int which.. ce je 0 = set, 1=exit
			    				
			    				if(which==0){
			    					startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
			    				}else{
			    					System.exit(0);
			    				}		
			    			}
			    		});
			    		AlertDialog alert=builder.create();
			    		alert.show();
			    			
			    		
			    	
			    	}
				}
			});
	        break;
	    
	    default:
	        dialog = null;
	    }
	      
	    return dialog;
	}
	private List<String> addressesToString(List<Address> addresses) {
		List<String> seznam = new LinkedList<String>();
			for(int i=0;i<addresses.size();i++){
				Address a = addresses.get(i);
				if(a.getCountryName()!=null && a.getLocality()!=null){
					String b =a.getCountryName()+" "+a.getLocality();
					seznam.add(b);
				}
				
			}
		return seznam;
	}
	
	/*
	 * Prikazemo dialog. Metoda showDialog klice onCreateDialog in s parametrom pove, kateri dialog zelimo izrisati. 
	 */
	private void enterLocation() {
		
		this.showDialog(0);
	}
	
	 /* ---------------------------------------------
	 * --------- END DESIRED LOCATION MODE ----------
	 * ----------------------------------------------
	 */
	
	/*
	 * Shranimo v XML
	 */
	private void endTracking(){
		
		try{
			
			this.gpsMng.stopListening();
			
			Road trackedRoad = this.roads.getFirst();
			trackedRoad.setTravelTime();
			
			Object[] params = new Object[] {this, trackedRoad.getmRoute().get(0), null};
			
			CompareRoutesAsync cmpRoutes = new CompareRoutesAsync(trackedRoad);
			cmpRoutes.execute(params);
			
			Object[] result = cmpRoutes.get();
			LinkedList<Road> roadsResult = (LinkedList<Road>)result[0];
			
			drawRoute(roadsResult);
			
			SaveToXML save = new SaveToXML(this);
			save.execute(this.roads);
			
		}catch(Exception e){
			Log.d("error", e.getMessage());
		}
	}	
	
	/*
	 * Prikazemo menu z opcijami. (klik na fizicen gumb moznosti)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.main_menu, menu);
		menu.findItem(R.id.menu_mode).setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			public boolean onMenuItemClick(MenuItem item) {
					transportDialog();
				return false;
			}

			
		});
		//menu.findItem(R.id.menu_settings).setIntent(new Intent(this, SettingsActivity.class));
		MenuItem endItem = menu.findItem(R.id.menu_end_track);
		if(Constants.runMode == Constants.START_MODE_TRACK){
			endItem.setVisible(true);
		}else{
			endItem.setVisible(false);
		}
		endItem.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			public boolean onMenuItemClick(MenuItem arg0) {
					endTracking();
				return false;
			}
		});
		
		
		return true;
		
	}
	
	
	/*
	 * 
	 */
	private void transportDialog() {
		final CharSequence[] items = {"Walk", "Bike", "Car", "Bus"};
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(true);
		builder.setOnCancelListener(new OnCancelListener() {
			
			public void onCancel(DialogInterface dialog) {
				if(roads.isEmpty()){
					Road road = new Road();
					road.setMode(2);
					roads.add(road);
				}else{
					roads.getFirst().setMode(2);
					roads.add(roads.getFirst());
				}
			}
		});
		builder.setTitle(R.string.means_transport);
		builder.setItems(items, new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				//torej ravnaj se po int which.. ce je 0 = Walk... 3 = Bus
				
				if(roads.isEmpty()){
					Road road = new Road();
					road.setMode(which);
					roads.add(road);
				}else{
					roads.getFirst().setMode(which);
					roads.add(roads.getFirst());
				}				
			}
		});
		AlertDialog alert=builder.create();
		alert.show();
				
	}
	
	/*
	 * Razsiritev AsyncTask razreda za pridobivanje naslovov. 
	 */
	private class AddressTask extends AsyncTask<Object, String, List<Address>>{

		@Override
		protected List<Address> doInBackground(Object... params) {
			
			CharSequence s = (CharSequence) params[0];
			
			Geocoder geocoder = new Geocoder(GoogleMapsActivity.this);
			
			String serchInput = s.toString();
			List<Address> pred = new LinkedList<Address>();
			
			try {
				pred=geocoder.getFromLocationName(serchInput, 5);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return pred;
		}
	}
		
	private class MyTextWatcher implements TextWatcher{

		ListView listView;
		Dialog dialog;
		
		public MyTextWatcher(ListView lv, Dialog dialog){
			this.listView = lv;
			this. dialog = dialog;
		}

		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}
		public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}
		public void afterTextChanged(Editable arg0) {}
	}
	
	/*
	 * Gre za metode, ki jih activity  mora imeti, ker implementira locationListener
	 * (non-Javadoc)
	 * @see android.location.LocationListener#onLocationChanged(android.location.Location)
	 */
	public void onLocationChanged(Location location) {}
	public void onProviderDisabled(String provider) {}
	public void onProviderEnabled(String provider) {}
	public void onStatusChanged(String provider, int status, Bundle extras) {}
}


