package com.carbon.tracker;
import java.util.LinkedList;

import com.carbon.tracker.db.xml.XML;
import com.carbon.tracker.entity.User;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {
	XML xmlHandler;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		xmlHandler = new XML(this);

		//if(checkInternetConnection(this))
			//return;
		
		LinkedList<User> users = null;
		try {
			users = xmlHandler.selectAllUsers();
		} catch (Exception c){ 
			Log.d("myError", c.toString());
		}
		
		if(users != null && users.size() > 0){
			Constants.user = users.getFirst();
			//Constants.user.select("*", "username = '"+Constants.user.getUsername()+"' AND password = '"+Constants.user.getPassword()+"'");
			String userName= Constants.user.getUsername();
			Toast.makeText(LoginActivity.this, "Welcome back "+userName, Toast.LENGTH_SHORT).show();
			LoginActivity.this.finish();
			startActivity(new Intent(LoginActivity.this, CarbonTrackerActivity.class));
			
		}else{
			//login / register
			
			Constants.firstTime = true;	
			
			Button register = (Button) findViewById(R.id.button_register);
			
			register.setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					if(checkInternetConnection(LoginActivity.this))
						return;
					EditText usrname = (EditText) findViewById(R.id.editText_username);
					String username = usrname.getText().toString();
					EditText pass = (EditText) findViewById(R.id.editText_pass);
					String password = pass.getText().toString();
					if(!(username.length() > 0 && password.length() > 0 )){
						Toast.makeText(LoginActivity.this, "Blank fields", Toast.LENGTH_SHORT).show();
					}
					if(!(username.contains("@")&& username.contains("."))){
						Toast.makeText(LoginActivity.this, "You have to enter email", Toast.LENGTH_SHORT).show();
						return;
					}
					User u = new User();
//					Connection conn = u.getConnection();
					Constants.CONN = User.getConnection();
				
					u.select("*", "username like '" + username + "' AND password like '" + password + "'");
					if (u.getUserId()==-1){
						
						User c = new User();
						c.select("*", "username like '"  + username + "'");
						
						if(c.getUserId()!=-1){
							Toast.makeText(LoginActivity.this, "Wrong password", Toast.LENGTH_SHORT).show();
							return;
						}
						
						User user = new User();
						user.setUsername(username);
						user.setPassword(password);
						user.insert();
						user.select("*", "password = '" + password + "' AND username = '" + username + "'");

						try {
							
							xmlHandler.addUser(user);
							Constants.user = user;
							
							LoginActivity.this.finish();
							startActivity(new Intent(LoginActivity.this, CarbonTrackerActivity.class));
						
						} catch (Exception e) {
							e.printStackTrace();
							Log.d("myError1",e.toString());
						}
					}else{
						try {
							xmlHandler.addUser(u);
							Constants.user = u;
							Toast.makeText(LoginActivity.this, "Welcome back", Toast.LENGTH_SHORT).show();
							LoginActivity.this.finish();
							startActivity(new Intent(LoginActivity.this, CarbonTrackerActivity.class));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
//					u.closeConnection();
				}
			});	
		}	
	}
	@Override
	protected void onResume() {
		//checkInternetConnection(LoginActivity.this);
		super.onResume();
	}
	public boolean checkInternetConnection(Context contex){
		ConnectivityManager conMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
    	
    	if(!(netInfo != null && netInfo.isConnected())){
    		final CharSequence[] items = {"Set my internet settings", "Exit"};
    		AlertDialog.Builder builder = new AlertDialog.Builder(contex);
    		builder.setCancelable(true);
    		
    		builder.setTitle(R.string.network_settings);
    		
    		builder.setItems(items, new DialogInterface.OnClickListener() {
    			public void onClick(DialogInterface dialog, int which) {
    				//torej ravnaj se po int which.. ce je 0 = set, 1=exit
    				
    				if(which==0){
    					startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
    				}else{
    					System.exit(0);
    				}		
    			}
    		});
    		AlertDialog alert=builder.create();
    		alert.show();
    	}else if(!(netInfo != null && netInfo.isConnectedOrConnecting())){
    		Toast.makeText(contex, "You are connecting", Toast.LENGTH_SHORT).show();
    		return true;
    	}else{
    		return false;
    	}
		return true;
	}
}
