package com.carbon.tracker.gps;

import android.location.Location;

public interface GPSCallback
{
        public abstract void onGPSUpdateDesired(Location location);
        
        public abstract void onGPSUpdateTracking(Location location);
}