
package com.carbon.tracker.gps;

import java.util.List;
import com.carbon.tracker.Constants;
import com.carbon.tracker.GoogleMapsActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class GPSManager
{
	public static int gpsMinTime;
	public static int gpsMinDistance;
	
	private static LocationManager locationManager = null;
	private static LocationListener locationListener = null;
	private static GPSCallback gpsCallback = null;
	ProgressDialog progresDialogWait;
	
	public int getDrawTime(){
		Constants.drawTime += 1;
		
		if(Constants.drawTime > 5)
			Constants.drawTime = 0;
		
		return Constants.drawTime;
	}

	public GPSManager()
	{	
		
		
		GPSManager.gpsMinDistance = Constants.GPS_MIN_DIST;
		GPSManager.gpsMinTime = Constants.GPS_MIN_TIME;

		GPSManager.locationListener = new LocationListener()
		{
			public void onLocationChanged(Location location)
			{
				int a = getDrawTime();
				if(a != 0)
					return;
				
				progresDialogWait.dismiss();
				if (GPSManager.gpsCallback != null)
				{
					if(GoogleMapsActivity.runMode == Constants.START_MODE_TRACK)
						GPSManager.gpsCallback.onGPSUpdateTracking(location);
					else if(GoogleMapsActivity.runMode == Constants.START_MODE_DES_LOC)
						GPSManager.gpsCallback.onGPSUpdateDesired(location);
				}
				
			}
			
		
			public void onProviderDisabled(String provider)
			{
			}
			
			
			public void onProviderEnabled(String provider)
			{
			}
			
			
			public void onStatusChanged(String provider, int status, Bundle extras)
			{
			}
		};
	}
	
	public GPSCallback getGPSCallback()
	{
		return GPSManager.gpsCallback;
	}
	
	public void setGPSCallback(final GPSCallback gpsCallback)
	{
		GPSManager.gpsCallback = gpsCallback;
	}
	
	public void startListening(final Context context)
	{
		progresDialogWait = ProgressDialog.show((Context) gpsCallback, "Getting your location", "Please wait");
		progresDialogWait.setCancelable(true);
		progresDialogWait.setOnCancelListener(new OnCancelListener() {
			
			public void onCancel(DialogInterface dialog) {
				progresDialogWait.dismiss();
				System.exit(0);
			}
		});
		
		
		if (GPSManager.locationManager == null)
		{
			GPSManager.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		}
		
		Criteria criteria = new Criteria();
		
		String bestProvider;
		
		if(GoogleMapsActivity.runMode == Constants.START_MODE_TRACK)
			bestProvider = LocationManager.GPS_PROVIDER;
			//bestProvider = GPSManager.locationManager.getBestProvider(criteria, true);
		
		else
			bestProvider = LocationManager.NETWORK_PROVIDER;
		
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setSpeedRequired(true);
		criteria.setAltitudeRequired(false);
		criteria.setBearingRequired(false);
		criteria.setCostAllowed(true);
		criteria.setPowerRequirement(Criteria.POWER_LOW);
		
		if (bestProvider != null && bestProvider.length() > 0)
		{
			GPSManager.locationManager.requestLocationUpdates(bestProvider, GPSManager.gpsMinTime, GPSManager.gpsMinDistance, GPSManager.locationListener);
		}
		else
		{
			List<String> providers = GPSManager.locationManager.getProviders(true);
			
			for (String provider : providers)
			{
				GPSManager.locationManager.requestLocationUpdates(provider, GPSManager.gpsMinTime, GPSManager.gpsMinDistance, GPSManager.locationListener);
			}
		}
	}
	
	public void stopListening()
	{
		try
		{
			if (GPSManager.locationManager != null && GPSManager.locationListener != null)
			{
				GPSManager.locationManager.removeUpdates(GPSManager.locationListener);
			}
			
			GPSManager.locationManager = null;
		}
		catch (final Exception ex)
		{
			
		}
	}
	
	/*
	 * getterji in setterji
	 */
	
	public int getGpsMinTime() {
		return gpsMinTime;
	}

	public void setGpsMinTime(int gpsMinTime) {
		GPSManager.gpsMinTime = gpsMinTime;
	}

	public int getGpsMinDistance() {
		return gpsMinDistance;
	}

	public void setGpsMinDistance(int gpsMinDistance) {
		GPSManager.gpsMinDistance = gpsMinDistance;
	}

	public static LocationManager getLocationManager() {
		return locationManager;
	}

	public static void setLocationManager(LocationManager locationManager) {
		GPSManager.locationManager = locationManager;
	}

	public static LocationListener getLocationListener() {
		return locationListener;
	}

	public static void setLocationListener(LocationListener locationListener) {
		GPSManager.locationListener = locationListener;
	}

	public static GPSCallback getGpsCallback() {
		return gpsCallback;
	}

	public static void setGpsCallback(GPSCallback gpsCallback) {
		GPSManager.gpsCallback = gpsCallback;
	}
}
