package com.carbon.tracker.entity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import android.util.Log;

public class MapPointDB {
	
	private  MapPoint mp;
	private  String statement;
	private  Connection conn;
	private  ResultSet rs;
	
	public MapPointDB(MapPoint mp) {
		this.setMp(mp);
	}

	public MapPoint getMp() {
		return mp;
	}

	public void setMp(MapPoint mp) {
		this.mp = mp;
	}
	
public  Connection getConnection() {
		
		String url = "jdbc:mysql://88.200.24.244:3306/";
		String dbName = "carbontracker";
		String driver = "com.mysql.jdbc.Driver";
		String userName = "testQuery"; 
		String password = "test";
		
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url+dbName,userName,password);
		 
			System.out.println("Connected to the database");
		  
			System.out.println("Disconnected from database");
		  	} catch (Exception e) {
		  		Log.d("myError",e.toString());
		  		e.printStackTrace();
		}
		return conn;
		
	}
	

	public ResultSet executeQuery(String sql) {
	Statement stmt;
	ResultSet rs=null; 
	try {
		stmt = conn.createStatement();
		
		rs = stmt.executeQuery(sql);
		setStatement(sql);
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	return rs;
	
	}
	
	public  void executeUpdate(String sql) {
		Statement stmt;
		try {
			stmt = conn.createStatement();
			
			stmt.executeUpdate(sql);
			setStatement(sql);
			
		} catch (SQLException e) {
			e.printStackTrace();
			
		}
		
	}
	

	public  void closeConnection() {
		try {
			conn.close();
		} catch (SQLException sqlE) {
		}
	}

//	public  void select(String column, String condition) {
//		getConnection();
//		rs = executeQuery("SELECT "+column+" FROM MapPoints WHERE "+condition);
//		next();
//		e.closeConnection();
//		
//	}
//	
//	public  void select(String column) {
//		e.getConnection();
//		rs = e.executeQuery("SELECT "+column+" FROM MapPoints");
//		next();
//			
//		e.closeConnection();
//		
//	}
	
	public  String getStatement() {
		return statement;
	}
	public  void setStatement(String statement) {
		this.statement = statement;
	}
	
	
	public  void insert() {
		getConnection();
		
		executeUpdate("INSERT INTO MapPoints (name, speed, Road_idRoad, latitude, longitude, TravelMode_idTravelMode)" +
					"VALUES+ ('"+mp.mName+"', "+mp.mSpeed+", "+mp.fkIdRoad+", "+mp.mLatitude+", "+mp.mLongitude+", "+mp.mTravelMode+")");
		
		closeConnection();
	}
	
	public  boolean rowChanged(MapPoint mp1) {
		
		if ( !mp1.mName.equals(mp.mName) || mp1.mSpeed != mp.mSpeed || mp1.fkIdRoad != mp.fkIdRoad || mp1.mLatitude != mp.mLatitude || mp1.mLongitude != mp.mLongitude || mp1.mTravelMode != mp.mTravelMode) {
			return true;
		}
		
		return false;
	}
	
	public  void update(MapPoint mp1) {
		if (rowChanged(mp)) {
			getConnection();
			
			executeUpdate("UPDATE MapPoints " +
							"SET name='"+mp1.mName+"', speed="+mp1.mSpeed+", Road_idRoad="+mp1.fkIdRoad+", latitude="+mp1.mLatitude+", longitude="+mp1.mLongitude+", TravelMode_idTravelMode="+mp1.mTravelMode+
							"WHERE name='"+mp.mName+"' AND speed="+mp.mSpeed+" AND Road_idRoad="+mp.fkIdRoad+" AND latitude="+mp.mLatitude+ " AND longitude="+mp.mLongitude+ " AND TravelMode_idTravelMode="+mp.mTravelMode);
			
			mp.mName = mp1.mName;
			mp.mSpeed = mp1.mSpeed;
			mp.fkIdRoad = mp1.fkIdRoad;
			mp.mLatitude = mp1.mLatitude;
			mp.mLongitude = mp1.mLongitude;
			mp.mTravelMode = mp1.mTravelMode;
			
			closeConnection();
		}
	}

	public void delete() {
		getConnection();
		
		executeUpdate("DELETE FROM MapPoints WHERE idMapPoints="+mp.id);
		
		closeConnection();
	}
	
}


