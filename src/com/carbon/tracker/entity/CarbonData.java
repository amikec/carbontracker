package com.carbon.tracker.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CarbonData extends Entity {
	
	private int carbonDataId;
	private int fkUser;
	private String emitterName;
	private int gramsCarbon;
	
	private int fkRoadId;
	
	private ResultSet rs;
	
	public CarbonData() {
		
	}
	
	public int getCarbonDataId() {
		return carbonDataId;
	}
	
	public void setCarbonDataId(int carbonDataId) {
		this.carbonDataId = carbonDataId;
	}
	
	public int getFkUser() {
		return fkUser;
	}
	
	public void setFkUser(int fkUser) {
		this.fkUser = fkUser;
	}
	
	public String getEmitterName() {
		return emitterName;
	}
	
	public void setEmitterName(String emitterName) {
		this.emitterName = emitterName;
	}
	
	public int getGramsCarbon() {
		return gramsCarbon;
	}
	
	public void setGramsCarbon(int gramsCarbon) {
		this.gramsCarbon = gramsCarbon;
	}
	
	public void select(String column, String condition) {
		getConnection();
		rs = executeQuery("SELECT "+column+" FROM CarbonData WHERE "+condition);
		next();
		closeConnection();
		
	}
	
	public void select(String column) {
		getConnection();
		rs = executeQuery("SELECT "+column+" FROM CarbonData");
		next();
			
		closeConnection();
		
	}
	
	public boolean next() {
		try {
			if (rs.next()) {
				rs.next();
				carbonDataId = rs.getInt("idCarbonData");
				fkUser = rs.getInt("Users_idUsers");
				emitterName = rs.getString("emitterName");
				gramsCarbon = rs.getInt("gramsCarbon");
				fkRoadId = rs.getInt("Road_idRoad");
				return true;
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
			
		}
		return false;
	}
	
	public void insert() {
		getConnection();
		
		executeUpdate("INSERT INTO CarbonData (emitterName, gramsCarbon, Users_idUsers, Road_idRoad)" +
					"VALUES ('"+emitterName+"', "+gramsCarbon+", "+fkUser+", "+fkRoadId+")");
		
		closeConnection();
	}
	
public boolean rowChanged(CarbonData cd) {
		
		if (!cd.getEmitterName().equals(emitterName) || cd.getGramsCarbon() != gramsCarbon || cd.getFkUser() != fkUser || cd.getFkRoadId() != fkRoadId) {
			return true;
		}
		
		return false;
	}
	
	public void update(CarbonData cd) {
		if (rowChanged(cd)) {
			getConnection();
			
			executeUpdate("UPDATE CarbonData " +
							"SET emitterName='"+cd.getEmitterName()+"', gramsCarbon="+cd.getGramsCarbon()+", Users_idUsers="+cd.getFkUser()+", Road_idRoad="+cd.getFkRoadId()+
							" WHERE emitterName='"+emitterName+"' AND gramsCarbon="+gramsCarbon+" AND Users_idUsers="+fkUser+ " AND Road_idRoad="+fkRoadId);
			
			emitterName = cd.getEmitterName();
			gramsCarbon = cd.getGramsCarbon();
			fkUser = cd.getFkUser();
			fkRoadId = cd.getFkRoadId();
			closeConnection();
		}
	}
	
	public void delete() {
		getConnection();
		
		executeUpdate("DELETE FROM CarbonData WHERE idCarbonData="+carbonDataId);
		
		closeConnection();
	}

	public int getFkRoadId() {
		return fkRoadId;
	}

	public void setFkRoadId(int fkRoadId) {
		this.fkRoadId = fkRoadId;
	}
	
}
