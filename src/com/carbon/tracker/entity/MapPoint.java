package com.carbon.tracker.entity;


/**
 * 
 * @author heox
 * Bean, ki hrani podatke o tockah, ki jih dobimo iz KML-ja
 */
public class MapPoint {
	
    public MapPoint(int latitudeE6, int longitudeE6) {
		this.mLatitude = latitudeE6 / 1E6;
		this.mLongitude = longitudeE6 / 1E6;
    }
  
	public int id;
	public String mName;
	public String mDescription;
	public double mSpeed;
	public double mLatitude;
	public double mLongitude;
	public int mTravelMode;
    
    public int fkIdRoad;
    
	public int getLatitudeE6(){
		return (int)(this.mLatitude * 1E6);
	}
	
	public int getLongitudeE6(){
		return (int)(this.mLongitude * 1E6);
	}
	
}