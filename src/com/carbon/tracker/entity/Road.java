package com.carbon.tracker.entity;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import com.carbon.tracker.Constants;

/**
 * 
 * @author heox
 * Bean, ki hrani podatke o poti iz KML-ja
 */
public class Road extends Entity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private int id;
	private String mName;
    private String mDescription;
    private int mWidth;
    private ArrayList<MapPoint> mRoute = new ArrayList<MapPoint>();
    private ArrayList<MapPoint> mPoints = new ArrayList<MapPoint>();
    private double lengthMeters = -1;
    private int travelTime;
    private int mode;
    private int mColor;
    private int fkUserId;
    private Date startTime;
	private java.sql.Timestamp sqlDate;
    private ResultSet rs;
    
    private Entity e;
    
    public Road(){
    	java.util.Date utilD = new java.util.Date();
    	this.sqlDate = new java.sql.Timestamp(utilD.getTime());
    }
    
    public Date getStartTime() {
		return startTime;
	}
	public void setStartTime() {
		this.startTime = new Date();
	}
    public int getmColor() {
		return mColor;
	}
	public void setmColor(int mColor) {
		this.mColor = mColor;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getmName() {
		return mName;
	}
	public void setmName(String mName) {
		this.mName = mName;
	}
	public String getmDescription() {
		return mDescription;
	}
	public void setmDescription(String mDescription) {
		this.mDescription = mDescription;
	}
	public int getmWidth() {
		return mWidth;
	}
	public void setmWidth(int mWidth) {
		this.mWidth = mWidth;
	}
	public ArrayList<MapPoint> getmRoute() {
		return mRoute;
	}
	public void setmRoute(ArrayList<MapPoint> mRoute) {
		this.mRoute = mRoute;
	}
	public ArrayList<MapPoint> getmPoints() {
		return mPoints;
	}
	public void setmPoints(ArrayList<MapPoint> mPoints) {
		this.mPoints = mPoints;
	}
	public double getLengthMeters() {
		if(lengthMeters == -1)
			this.lengthMeters = calculateRoadLength();
		
		return lengthMeters;
	}
	public void setLengthMeters(double lengthMeters) {
		this.lengthMeters = lengthMeters;
	}
	public int getMode() {
		return mode;
	}
	public void setMode(int mode) {
		this.mode = mode;
	}
	
	public int getTravelTime() {
		return this.travelTime;
	}
	public void setTravelTime() {
		this.travelTime = (new Date()).getMinutes() - this.getStartTime().getMinutes();
		
	}
	public void setTravelTime(int travelTime) {
		this.travelTime = travelTime;
	}
	public int getFkUserId() {
		return fkUserId;
	}
	public void setFkUserId(int fkUserId) {
		this.fkUserId = fkUserId;
	}
	
	public double calculateEmittingLength(){
		double radius = 6371;
		double lat1, lat2, lon1, lon2, dLat, dLon, c, a;
		double result = 0;
		for (int i = 0; i < mRoute.size()-1; i++) {
		
			MapPoint startP = mRoute.get(i);
			MapPoint endP = mRoute.get(i+1);
			
			if(startP.mTravelMode == Constants.TRAVEL_MODE_WALK)
				continue;
			
			lat1 = startP.mLatitude;
			lat2 = endP.mLatitude;
			lon1 = startP.mLongitude;
			lon2 = endP.mLongitude;
			dLat = Math.toRadians(lat2-lat1);
			dLon = Math.toRadians(lon2-lon1);
			
			a = Math.sin(dLat/2) * Math.sin(dLat/2) +
					Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
					Math.sin(dLon/2) * Math.sin(dLon/2);
			c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			
			result += radius * c;
		}
		
		return result;
	}
	
	public double calculateRoadLength() {

		double radius = 6371;
		double lat1, lat2, lon1, lon2, dLat, dLon, c, a;
		double result = 0;
		for (int i = 0; i < mRoute.size()-1; i++) {
			MapPoint startP = mRoute.get(i);
			MapPoint endP = mRoute.get(i+1);
			
			lat1 = startP.mLatitude;
			lat2 = endP.mLatitude;
			lon1 = startP.mLongitude;
			lon2 = endP.mLongitude;
			dLat = Math.toRadians(lat2-lat1);
			dLon = Math.toRadians(lon2-lon1);
			
			a = Math.sin(dLat/2) * Math.sin(dLat/2) +
					Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
					Math.sin(dLon/2) * Math.sin(dLon/2);
			c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			
			result += radius * c;
		}
		
		return result*1000;
	}
    
	public void select(String column, String condition) {
		try {
			if (Constants.CONN == null || Constants.CONN.isClosed()) {
				Constants.CONN = getConnection();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		rs = executeQuery("SELECT "+column+" FROM road WHERE "+condition);
		next();
		
		
	}
	
	public void select(String column) {
		try {
			if (Constants.CONN == null || Constants.CONN.isClosed()) {
				Constants.CONN = getConnection();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		rs = executeQuery("SELECT "+column+" FROM road");
		next();
			
		
		
	}
	
	public void next() {
		try {
			if (rs.next()) {
				rs.next();
				id = rs.getInt("idRoad");
				lengthMeters = rs.getDouble("length");
				travelTime = rs.getInt("password");
				mode = rs.getInt("TravelMode_idTravelMode");
				fkUserId = rs.getInt("Users_idUsers");
				/*
				// manjka handlanje mappointov
				MapPoint mp = new MapPoint(0,0);
				mp.select("*", "Road_idRoad="+id);
				mRoute.add(mp);
				
				while(mp.next()) {
					mRoute.add(mp);
				}*/
				
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	
	public boolean last() {
		try {
			if (rs.last()) {
				id = rs.getInt("idRoad");
				lengthMeters = rs.getDouble("length");
				travelTime = rs.getInt("travelTime");
				mode = rs.getInt("TravelMode_idTravelMode");
				fkUserId = rs.getInt("Users_idUsers");
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public void insert() {
		try {
			if (Constants.CONN == null || Constants.CONN.isClosed()) {
				Constants.CONN = getConnection();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		executeUpdate("INSERT INTO road (Users_idUsers, length, TravelMode_idTravelMode, travelTime, date)" +
					"VALUES ("+fkUserId+", "+lengthMeters+", "+mode+", "+travelTime+", '"+sqlDate+"')");
		
		Road r = new Road();
		r.select("*", "Users_idUsers = "+fkUserId+" AND length =" + lengthMeters);
		r.last();
		
		for (MapPoint mp : mRoute) {
			mp.fkIdRoad = r.id;
			executeUpdate("INSERT INTO mappoints (name, speed, Road_idRoad, latitude, longitude, TravelMode_idTravelMode)" +
				" VALUES ('" + mp.mName + "', " + mp.mSpeed + ", " + mp.fkIdRoad + ", " + mp.mLatitude+", "+mp.mLongitude+", "+mp.mTravelMode+")");
		}
		
	}
	
	public boolean rowChanged(Road r) {
		
		if (r.getFkUserId() != fkUserId || r.getLengthMeters() != lengthMeters || r.mode != mode || r.travelTime != travelTime) {
			return true;
		}
		
		return false;
	}
	
	public void update(Road r) {
		if (rowChanged(r)) {
			try {
				if (Constants.CONN == null || Constants.CONN.isClosed()) {
					Constants.CONN = getConnection();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			executeUpdate("UPDATE road " +
							"SET Users_idUsers="+r.fkUserId+", length="+r.lengthMeters+", TravelMode_idTravelMode="+r.mode+", travelTime="+r.travelTime+
							"WHERE Users_idUsers="+fkUserId+" AND length="+lengthMeters+" AND TravelMode_idTravelMode="+mode+" AND travelTime="+travelTime);
			
			fkUserId = r.fkUserId;
			lengthMeters = r.lengthMeters;
			mode = r.mode;
			travelTime = r.travelTime;
			
			
		}
	}
	
	public void delete() {
		try {
			if (Constants.CONN == null || Constants.CONN.isClosed()) {
				Constants.CONN = getConnection();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		executeUpdate("DELETE FROM road WHERE idRoad="+id);
		
		
	}
	public Timestamp getSqlDate() {
		return sqlDate;
	}
	public void setSqlDate(Timestamp sqlDate) {
		this.sqlDate = sqlDate;
	}
	
	
    
}