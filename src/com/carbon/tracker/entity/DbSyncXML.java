package com.carbon.tracker.entity;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;

import com.carbon.tracker.Constants;
import com.carbon.tracker.db.xml.XML;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class DbSyncXML extends AsyncTask<Void, Object, Object>{

	private XML xmlHandler;
	private Context context;
	
	public DbSyncXML(Context context){
		this.context = context;
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 */
	@Override
	protected Object doInBackground(Void... arg0) {
		
		try {
		
			xmlHandler = new XML(context);
			
			LinkedList<Road> roads = null;
			roads = xmlHandler.SelectAllRoads();
//			Connection conn = null;
			if (Constants.CONN==null || Constants.CONN.isClosed()) {
				Constants.CONN = Entity.getConnection();
			}
			
			for (Road road : roads) {
				road.setmRoute(new ArrayList<MapPoint>(xmlHandler.selectMapPoints(road.getId())));
				road.insert();
			}
//			if (conn != null) {
//				conn.close();
//			}
			UserSettings s = xmlHandler.selectSettings();
			User u = Constants.user;
			s.insertOrUpdate(s);
			Entity.closeConnection();
			String path = context.getFilesDir()+Constants.FILE_DB;

			File f = new File(path);
			
			f.delete();

			xmlHandler.init();
			xmlHandler.addUser(u);
			xmlHandler.addSettings(s);
			
			
		} catch (Exception ex) {
			Log.d("error",ex.toString());
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see android.os.AsyncTask#onPreExecute()
	 */
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.os.AsyncTask#onProgressUpdate(Progress[])
	 */
	@Override
	protected void onProgressUpdate(Object... values) {
		super.onProgressUpdate(values);
	}
	
	@Override
	protected void onPostExecute(Object result) {
		super.onPostExecute(result);
	}
	
	@Override
	protected void onCancelled() {
		super.onCancelled();
	}
}
