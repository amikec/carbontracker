package com.carbon.tracker.entity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.carbon.tracker.Constants;

public class UserSettings extends Entity{
	private int id;
	private String manufacturer;
	private String ccm;
	private String fuel;
	private String modelName;
	private String makeNum;
	private int syncOn;
	private int userId;
	private double gramsPerKm;
	private ResultSet rs;
	private Connection conn;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getCcm() {
		return ccm;
	}
	public void setCcm(String ccm) {
		this.ccm = ccm;
	}
	public String getFuel() {
		return fuel;
	}
	public void setFuel(String fuel) {
		this.fuel = fuel;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getMakeNum() {
		return makeNum;
	}
	public void setMakeNum(String makeNum) {
		this.makeNum = makeNum;
	}
	public int getSyncOn() {
		return syncOn;
	}
	public void setSyncOn(int syncOn) {
		this.syncOn = syncOn;
	}
	public double getGramsPerKm() {
		return gramsPerKm;
	}
	public void setGramsPerKm(double gramsPerKm) {
		this.gramsPerKm = gramsPerKm;
	}

	public UserSettings select(int userID){
		rs = executeQuery("SELECT * FROM user_settings" +
							" WHERE users_idUsers="+userID);
		try {
			if (rs.first()) {
				this.manufacturer = rs.getString("manufacturer");
				this.ccm = rs.getString("ccm");
				this.fuel = rs.getString("fuel");
				this.modelName = rs.getString("modelName");
				this.makeNum = rs.getString("makeNum");
				this.syncOn = rs.getInt("syncOn");
				this.userId = rs.getInt("users_idUsers");
				this.gramsPerKm = rs.getDouble("gramsPerKm");
			}else{
				this.id = -1;
			}

		} catch (SQLException e) {
			
			this.id = -1;
			
			id = -1;
			e.printStackTrace();
		}
		
	
		
		return this;

	}
	
	public void insert(){
//		getConnection();
		
		executeUpdate("INSERT INTO user_settings (manufacturer, ccm, fuel, modelName, makeNum, syncOn, gramsPerKm, users_idUsers)" +
						" VALUES ('"+manufacturer+"', '"+ccm+"', '"+fuel+"', '"+modelName+"', '"+makeNum+"', "+syncOn+", "+gramsPerKm+", "+userId+")");
		
//		closeConnection();
	}
	
	public void update(UserSettings us) {
//		getConnection();
		
		executeUpdate("UPDATE user_settings " +
						"SET manufacturer='"+us.getManufacturer()+"', ccm='"+us.getCcm()+"', fuel='"+us.getFuel()+"', modelName='"+us.getModelName()+"', makeNum='"+us.getMakeNum()+"', syncOn="+us.getSyncOn()+", users_idUsers="+us.getUserId()+", gramsPerKm="+us.getGramsPerKm()+
						" WHERE manufacturer='"+manufacturer+"' AND ccm='"+ccm+"', fuel='"+fuel+"', modelName='"+modelName+"', makeNum='"+makeNum+"', syncOn="+syncOn+", users_idUsers="+userId+", gramsPerKm="+gramsPerKm);
		
//		closeConnection();
	}
	
	public void insertOrUpdate(UserSettings us) {
//		conn = getConnection();
//		UserSettings result = select(us.getUserId());
		
		try {
			if (Constants.CONN == null || Constants.CONN.isClosed()) {
				Constants.CONN = getConnection();
				conn = Constants.CONN;
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		rs = executeQuery("SELECT * FROM user_settings" +
				" WHERE users_idUsers="+us.getUserId());
		try {
		if (rs.first()) {
			this.manufacturer = rs.getString("manufacturer");
			this.ccm = rs.getString("ccm");
			this.fuel = rs.getString("fuel");
			this.modelName = rs.getString("modelName");
			this.makeNum = rs.getString("makeNum");
			this.syncOn = rs.getInt("syncOn");
			this.userId = rs.getInt("users_idUsers");
			this.gramsPerKm = rs.getDouble("gramsPerKm");
		}else{
			this.id = -1;
		}
		
		} catch (SQLException e) {
		
		this.id = -1;
		
		id = -1;
		e.printStackTrace();
		}
		
		
		
		if (this.getId() == -1) {
			us.insert();
		} else {
			this.update(us);
		}
//		try {
//			conn.close();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
}
