package com.carbon.tracker.entity;

import java.util.LinkedList;

import com.carbon.tracker.Constants;
import com.carbon.tracker.db.xml.XML;

import android.content.Context;
import android.os.AsyncTask;

public class SaveToXML  extends AsyncTask<Object, Object, Object>{

	private LinkedList<Road> roads;
	private Context context;
	private XML xmlHandler;
	
	public SaveToXML(Context context){
		this.context = context;
	}
	
	@Override
	protected Object doInBackground(Object... arg0) {
		
		try{
			
			xmlHandler = new XML(context);
		
			this.roads = (LinkedList<Road>) arg0[0];
			
			Road road = roads.getFirst();
			
			int id = xmlHandler.addRoad(Constants.user.getUserId(), road.getLengthMeters(), road.getMode(), road.getSqlDate().toString(), road.getTravelTime());
			road.setId(id);
			
			for(MapPoint mp : road.getmRoute()){
				int idMp = xmlHandler.addMap("sasasa", mp.mSpeed, road.getId(), mp.mLatitude, mp.mLongitude, mp.mTravelMode);
				mp.id = idMp;
			}
		
		}catch (Exception e){
			
			e.printStackTrace();
			
			//ROLLBACK
			Road dRoad = roads.getFirst();
				
			try{
				
				xmlHandler.deleteRoad(dRoad.getId());
				
				for(MapPoint dMp : dRoad.getmRoute()){
					xmlHandler.deleteRoad(dMp.id);
				}	
				
			}catch(Exception e2){
				e.printStackTrace();
			}
			
		}
		
		return null;
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.os.AsyncTask#onPreExecute()
	 */
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.os.AsyncTask#onProgressUpdate(Progress[])
	 */
	@Override
	protected void onProgressUpdate(Object... values) {
		super.onProgressUpdate(values);
	}
	
	@Override
	protected void onPostExecute(Object result) {
		super.onPostExecute(result);
	}
	
	@Override
	protected void onCancelled() {
		super.onCancelled();
	}
}
