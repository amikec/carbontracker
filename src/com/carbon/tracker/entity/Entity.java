package com.carbon.tracker.entity;


import java.sql.*;

import com.carbon.tracker.Constants;

import android.util.Log;

public class Entity {
	private static Connection conn = Constants.CONN;
	private String statement = null;
	
	public static Connection getConnection() {
		
		String url = "jdbc:mysql://88.200.24.244:3306/";
		String dbName = "carbontracker";
		String driver = "com.mysql.jdbc.Driver";
		String userName = "testQuery"; 
		String password = "test";
		
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url+dbName,userName,password);
			conn.setAutoCommit(false);
		 
		  	} catch (Exception e) {
		  		Log.d("myError",e.toString());
		  		e.printStackTrace();
		}
		return conn;
		
	}
	
	public static Connection getActiveConnection() {
		if (conn != null) {
			return conn;
		}
		
		return null;
	}
	
	public static void closeConnection() {
		try {
			conn.commit();
			conn.close();
		} catch (SQLException sqlE) {
		}
	}
	
	public ResultSet executeQuery(String sql) {
		try {
			if (conn == null || conn.isClosed()) {
				Constants.CONN = getConnection();
				conn = Constants.CONN;
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Statement stmt;
		ResultSet rs=null; 
		try {
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery(sql);
			setStatement(sql);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rs;
		
	}
	
	public void executeUpdate(String sql) {
		Statement stmt;
	//	getConnection();
		try {
			if (conn == null || conn.isClosed()) {
				Constants.CONN = getConnection();
				conn = Constants.CONN;
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			
			stmt = conn.createStatement();
			
			stmt.executeUpdate(sql);
			setStatement(sql);
			
		} catch (SQLException e) {
			e.printStackTrace();
			
		}
		
	}
	public String getStatement() {
		return statement;
	}
	public void setStatement(String statement) {
		this.statement = statement;
	}
}
