package com.carbon.tracker.entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class User extends Entity {
	
	private int userId;
	private String username;
	private String password;
	
	private ArrayList<CarbonData> carbonData;
	private ResultSet rs;
	
	public int getUserId() {
		return userId;
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public ArrayList<CarbonData> getCarbonData() {
		return carbonData;
	}
	
	public void setCarbonData(ArrayList<CarbonData> carbonData) {
		this.carbonData = carbonData;
	}
	
	public void select(String column, String condition) {
		//getConnection();
		rs = executeQuery("SELECT "+column+" FROM users WHERE "+condition);
		try {
			if (rs.next()) {
				userId = rs.getInt("idUsers");
				username = rs.getString("username");
				password = rs.getString("password");
			} else {
				userId = -1;
				username = null;
				password = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//closeConnection();
	}
	
	public void select(String column) {
		//getConnection();
		rs = executeQuery("SELECT "+column+" FROM users");
		try {
			if (rs.next()) {
				userId = rs.getInt("idUsers");
				username = rs.getString("username");
				password = rs.getString("password");
			} else {
				userId = -1;
				username = null;
				password = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//closeConnection();
	}
	
	public boolean next() {
		try {
			if (rs.next()) {
				userId = rs.getInt("idUsers");
				username = rs.getString("username");
				password = rs.getString("password");
				
				CarbonData cd = new CarbonData();
				cd.select("*", "Users_idUsers="+userId);
				carbonData.add(cd);
				
				while(cd.next()) {
					carbonData.add(cd);
				}
				return true;
			}
			else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void insert() {
		//getConnection();
		
		executeUpdate("INSERT INTO users (username, password)" +
					"VALUES ('"+username+"', '"+password+"')");
		
		
		
		//closeConnection();
	}
	
	public boolean rowChanged(User u) {
		
		if (!u.getUsername().equals(username) || !u.getPassword().equals(username)) {
			return true;
		}
		
		return false;
	}
	
	public void update(User u) {
		if (rowChanged(u)) {
			//getConnection();
			
			executeUpdate("UPDATE users " +
							"SET username='"+u.username+"', password='"+u.password+"'"+
							"WHERE username='"+username+"' AND password='"+password+"'");
			username = u.username;
			password = u.password;
			//closeConnection();
		}
	}
	
	public void delete() {
		//getConnection();
		
		executeUpdate("DELETE FROM users WHERE idUsers="+userId);
		
		//closeConnection();
	}
}
