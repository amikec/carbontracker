package com.carbon.tracker.map;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import com.carbon.tracker.Constants;
import com.carbon.tracker.GoogleMapsActivity;
import com.carbon.tracker.R;
import com.carbon.tracker.entity.MapPoint;
import com.carbon.tracker.entity.Road;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

/*
 * Razred za risanje po mapi
 */
public class MapOverlay extends com.google.android.maps.Overlay {

    Road mRoad;
    ArrayList<MapPoint> mPoints;
    Context context;
    
    
    public MapOverlay(Context context, MapView mv){
    	this.context = context;
    }
    
    public MapOverlay(Context context, Road road, MapView mv) {
        mRoad = road;
        mPoints = mRoad.getmRoute();
        this.context = context;
        
        if (road.getmRoute().size() > 0 && GoogleMapsActivity.runMode == Constants.START_MODE_DES_LOC) {
                
            int moveToLat = (mPoints.get(0).getLatitudeE6() + 
            				(mPoints.get(mPoints.size() - 1).getLatitudeE6() - 
                            mPoints.get(0).getLatitudeE6()) / 2);
            
            int moveToLong = (mPoints.get(0).getLongitudeE6() + (mPoints.get(
                            mPoints.size() - 1).getLongitudeE6() - 
                            mPoints.get(0).getLongitudeE6()) / 2);
            
            GeoPoint moveTo = new GeoPoint(moveToLat, moveToLong);

            MapController mapController = mv.getController();
            mapController.animateTo(moveTo);
            mapController.setZoom(10);
        }
    }

    @Override
    public boolean draw(Canvas canvas, MapView mv, boolean shadow, long when) {
        super.draw(canvas, mv, shadow);
        
        if(this.mRoad != null)
        	drawPath(mv, canvas);
        
        return true;
    }

    public void drawPath(MapView mv, Canvas canvas) {
    	
		ArrayList<MapPoint> mPoints = mRoad.getmRoute();
	
        int x1 = -1, y1 = -1, x2 = -1, y2 = -1;
        Paint paint = new Paint();
        
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(4);
        
        for (int i = 0; i < mPoints.size(); i++) {
            Point point = new Point();
            MapPoint mp = mPoints.get(i);
            GeoPoint g = new GeoPoint(mp.getLatitudeE6(), mp.getLongitudeE6());
            
            paint.setColor(getColor(mPoints.get(i).mTravelMode));
            mv.getProjection().toPixels(g, point);
            x2 = point.x;
            y2 = point.y;
            
            if (i > 0) 
                    canvas.drawLine(x1, y1, x2, y2, paint);
            
            x1 = x2;
            y1 = y2;
        }
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event, MapView mapView) 
    {   
    	if (event.getAction() == 1) {                
            GeoPoint p = mapView.getProjection().fromPixels((int) event.getX(), (int) event.getY());
            
            if(GoogleMapsActivity.runMode == Constants.START_MODE_DES_LOC && GoogleMapsActivity.endPoint == null){
            	Drawable drawable = context.getResources().getDrawable(R.drawable.androidmarker);
            	ItemizedOverlayImpl itemizedoverlay = new ItemizedOverlayImpl(drawable, context);  
            	OverlayItem overlayitem = new OverlayItem(p, "End location", "This is the place you'd like to go to..");
            	itemizedoverlay.addOverlay(overlayitem);
            	mapView.getOverlays().add(itemizedoverlay);
            }
        }                         
        return false;
    }
    
    
    private int getColor(int mode){
    	switch (mode) {
		case Constants.TRAVEL_MODE_WALK:
			return Color.GREEN;
		case Constants.TRAVEL_MODE_BIKE:
			return Color.BLUE;
		case Constants.TRAVEL_MODE_CAR:
			return Color.RED;
		case Constants.TRAVEL_MODE_BUS:
			return Color.YELLOW;
		case Constants.TRAVEL_MODE_MOTORBIKE: 
			return Color.MAGENTA;
		default:
			return Color.BLUE;

		}
    }
}
