package com.carbon.tracker.map;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.json.JSONArray;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.carbon.tracker.Constants;
import com.carbon.tracker.entity.MapPoint;
import com.carbon.tracker.entity.Road;

/*
 * Razred za rokovanje s KML podatki
 */
public class RoadProvider {
	
		public static Road getRouteData(MapPoint start, MapPoint end, int mode){
			return getRouteData(start.getLatitudeE6()  / 1E6,
								start.getLongitudeE6() / 1E6,
								end.getLatitudeE6()    / 1E6,
								end.getLongitudeE6()   / 1E6,
								mode);
		}
	
		public static Road getRouteData(double sLat, double sLong, double eLat, double eLong, int mode){
            
			Road mRoad = new Road();
			
			try{
			
				String urlString = RoadProvider.getUrl(sLat, sLong, eLat, eLong, mode);
				
				//Delimo glede na tip podatkov
				if(Constants.DATA_TYPE.equals("json")){
					mRoad = parseJSON(urlString, mode);
					mRoad.setMode(mode);
				}else if(Constants.DATA_TYPE.equals("kml")){	
					InputStream is = getConnection(urlString);
					mRoad = RoadProvider.getRoute(is);
					mRoad.setMode(mode);
				}				
				
			}catch (Exception e){
				e.printStackTrace();
			}
			
            return mRoad;

		}
		
        public static InputStream getConnection(String url) {
            InputStream is = null;
            try {
                    URLConnection conn = new URL(url).openConnection();
                    is = conn.getInputStream();
                    
            } catch (MalformedURLException e) {
                    e.printStackTrace();
            } catch (IOException e) {
                    e.printStackTrace();
            }
            return is;
        }
	
        /*
         * Ce parsamo KML
         */
        public static Road getRoute(InputStream is) {
        	
                KMLHandler handler = new KMLHandler();
                try {
                    	SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
                        parser.parse(is, handler);
                } catch (ParserConfigurationException e) {
                        e.printStackTrace();
                } catch (SAXException e) {
                        e.printStackTrace();
                } catch (IOException e) {
                        e.printStackTrace();
                }
                return handler.mRoad;
        }
        
        /*
         * Zgradimo url za google maps service
         */
        public static String getUrl(double fromLat, double fromLon, double toLat, double toLon, int mode) {
			// connect to map web service
			StringBuffer urlString = new StringBuffer();
			
			if(mode != Constants.TRAVEL_MODE_BUS){
				urlString.append("http://maps.googleapis.com/maps/api/directions/json?sensor=false&units=metric");
				urlString.append("&origin=");// from
				urlString.append(Double.toString(fromLat));
				urlString.append(",");
				urlString.append(Double.toString(fromLon));
				urlString.append("&destination=");// to
				urlString.append(Double.toString(toLat));
				urlString.append(",");	
				urlString.append(Double.toString(toLon));
				
				if(mode == Constants.TRAVEL_MODE_WALK )
					urlString.append("&mode=walking");
				else if(mode == Constants.TRAVEL_MODE_CAR)
					urlString.append("&mode=driving");
			}else{
				urlString.append("http://maps.google.com/maps?f=d&hl=en");
				urlString.append("&saddr=");// from
				urlString.append(Double.toString(fromLat));
				urlString.append(",");
				urlString.append(Double.toString(fromLon));
				urlString.append("&daddr=");// to
				urlString.append(Double.toString(toLat));
				urlString.append(",");	
				urlString.append(Double.toString(toLon));
				urlString.append("&dirflg=r&hl=en&ie=UTF8&z=14&output=" + Constants.DATA_TYPE);
			}
			
		return urlString.toString();
        }
        
        /*
         * Metoda za branje JSON objekta
         */
        private static Road parseJSON(String urlString, int mode){
        	
        	ArrayList<MapPoint> markersList = new ArrayList<MapPoint>();
        	Road road = new Road();
        	String points = "";
        	String group = "";
        	
    		try{    			
    			
    			StringBuilder response = new StringBuilder();
    			
    		    URL url = new URL(urlString);
    		    HttpURLConnection httpconn = (HttpURLConnection)url.openConnection();
    		    
    		    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK)
    		    {
    		        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()),8192);
    		        String strLine = null;
    		        while ((strLine = input.readLine()) != null)
    		        {
    		            response.append(strLine);
    		        }
    	  	        input.close();
    	  	    }
    		    
    		    String jsonOutput = response.toString().replace("while(1);", "");
    		    
    		    JSONObject jsonObject = new JSONObject(jsonOutput);
    		    
    		    if(mode == Constants.TRAVEL_MODE_BUS){
    		    	JSONObject transit = jsonObject.getJSONObject("transit");
    		    	JSONArray polylines = transit.getJSONArray("routes").getJSONObject(0).getJSONArray("polylines");
    		    	for(int i = 0; i < polylines.length(); i++){
    		    		JSONObject line = polylines.getJSONObject(i);
    		    		points = line.getString("points");
    		    		group = line.getString("group");
    		    		
    		    		if(group.equals("vehicle"))
    		    			decodePolyLine(points, road, 0, Constants.TRAVEL_MODE_BUS);
    		    		else if(group.equals("walk"))
    		    			decodePolyLine(points, road, 0, Constants.TRAVEL_MODE_WALK);
    		    		else
    		    			decodePolyLine(points,road, 0, mode);
    		    	}
    		    	
    		    	road.setLengthMeters(road.calculateRoadLength());
    		    }else{
	    		    
	    		    JSONArray jsonRoute = jsonObject.getJSONArray("routes").getJSONObject(0).getJSONArray("legs");
	    		    JSONObject jsonLeg = jsonRoute.getJSONObject(0);
	    		    
	    		    int duration = jsonLeg.getJSONObject("duration").getInt("value");
	    		    int distance = jsonLeg.getJSONObject("distance").getInt("value");
	    		    
	    		    road.setLengthMeters(distance);
	    		    road.setTravelTime(duration);
	    		    
	    		    JSONArray jsonSteps = jsonLeg.getJSONArray("steps");
	    		    JSONObject step;
	    		    
	    		    for(int i = 0; i < jsonSteps.length(); i++){
	    		    	step = jsonSteps.getJSONObject(i);
	    		    	points = step.getJSONObject("polyline").getString("points").toString();
	    		    	decodePolyLine(points, road, 0, mode);
	    		    }
	    		    
	    		    
    		    }

	        }catch(Exception e){
	        	e.printStackTrace();
	        }
    		
    		return road;
    	}
        
    	public static int getPointNum(){
    		Constants.directionsPoints += 1;
    		
    		if(Constants.directionsPoints > 5)
    			Constants.directionsPoints = 0;
    		
    		return Constants.directionsPoints;
    	}

        
        private static void decodePolyLine(String poly, Road road, int array, int mode) {

			int len = poly.length();
			int index = 0;
			int lat = 0;
			int lng = 0;
			
			while (index < len) {
				int b;
				int shift = 0;
				int result = 0;
				
				do {
					b = poly.charAt(index++) - 63;
					result |= (b & 0x1f) << shift;
					shift += 5;
				} while (b >= 0x20);
					
				int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
				lat += dlat;
				shift = 0;
				result = 0;
				
				do {
					b = poly.charAt(index++) - 63;
					result |= (b & 0x1f) << shift;
					shift += 5;
				} while (b >= 0x20);
					
				int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
				lng += dlng;
				
				MapPoint mp = new MapPoint((int) ((lat / 1E5) * 1E6) ,(int) ((lng / 1E5) * 1E6));
				mp.mTravelMode = mode;
				
				if(array == 0)
					road.getmRoute().add(mp);
				else if(array == 2)
					road.getmPoints().add(mp);
			}	
		}
	}


/*
 * Parser
 */
class KMLHandler extends DefaultHandler {
        Road mRoad;
        boolean isPlacemark;
        boolean isRoute;
        boolean isItemIcon;
        private Stack<String> mCurrentElement = new Stack<String>();
        private String mString;

        public KMLHandler() {
                mRoad = new Road();
        }

        public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
                mCurrentElement.push(localName);
                
                if (localName.equalsIgnoreCase("Placemark")) {
                		mRoad.getmPoints().add(new MapPoint(0,0));
                        isPlacemark = true;
                } else if (localName.equalsIgnoreCase("ItemIcon")) {
                        if (isPlacemark)
                                isItemIcon = true;
                }
                
                mString = new String();
        }

        public void characters(char[] ch, int start, int length) throws SAXException {
                String chars = new String(ch, start, length).trim();
                mString = mString.concat(chars);
        }

        public void endElement(String uri, String localName, String name) throws SAXException {
        	
                if (mString.length() > 0) {
                        if (localName.equalsIgnoreCase("name")) {
                                if (isPlacemark) {
                                        isRoute = mString.equalsIgnoreCase("Route");
                                        
                                        if (!isRoute) {
                                                //mRoad.getmPoints().get(mRoad.getmPoints().size() - 1).setmName(mString);
                                        }
                                        
                                } else {
                                        mRoad.setmName(mString);
                                }
                        } else if (localName.equalsIgnoreCase("color") && !isPlacemark) {
                                mRoad.setmColor(Integer.parseInt(mString, 16));
                        } else if (localName.equalsIgnoreCase("width") && !isPlacemark) {
                                mRoad.setmWidth(Integer.parseInt(mString));
                        } else if (localName.equalsIgnoreCase("description")) {
                                if (isPlacemark) {
                                        String description = cleanup(mString);
                                        
                                        if (!isRoute)
                                        		mRoad.getmPoints().get(mRoad.getmPoints().size() - 1).mDescription = description;
                                        else
                                                mRoad.setmDescription(description);
                                }
                        } else if (localName.equalsIgnoreCase("href")) {
                                if (isItemIcon) {
                                	//mRoad.getmPoints().get(mRoad.getmPoints().size() - 1).setmIconUrl(mString);
                                }
                        } else if (localName.equalsIgnoreCase("coordinates")) {
                                if (isPlacemark) {
                                        if (!isRoute) {
                                                String[] xyParsed = split(mString, ",");
                                                double lon = Double.parseDouble(xyParsed[0]);
                                                double lat = Double.parseDouble(xyParsed[1]);
                                                mRoad.getmPoints().get(mRoad.getmPoints().size() - 1).mLatitude = lat;
                                                mRoad.getmPoints().get(mRoad.getmPoints().size() - 1).mLongitude = lon;
                                        } else {
                                                String[] coodrinatesParsed = split(mString, " ");
                                                //mRoad.mRoute = new double[coodrinatesParsed.length][2];
                                                
                                                for (int i = 0; i < coodrinatesParsed.length; i++) {
                                                        String[] xyParsed = split(coodrinatesParsed[i], ",");
                                                        
                                                        for (int j = 0; j < 2 && j < xyParsed.length; j+=2){
                                                        		double lon = Double.parseDouble(xyParsed[j]);
                                                        		double lat = Double.parseDouble(xyParsed[j+1]);
                                                        		MapPoint p = new MapPoint((int)(lat*1E6), (int)(lon*1E6));
                                                        		mRoad.getmRoute().add(p);
                                                        }	
                                                }
                                        }
                                }
                        }
                }
                
                mCurrentElement.pop();
                
                if (localName.equalsIgnoreCase("Placemark")) {
                        isPlacemark = false;
                        if (isRoute)
                                isRoute = false;
                } else if (localName.equalsIgnoreCase("ItemIcon")) {
                        if (isItemIcon)
                                isItemIcon = false;
                }
        }

        private String cleanup(String value) {
                String remove = "<br/>";
                int index = value.indexOf(remove);
                
                if (index != -1)
                        value = value.substring(0, index);
                
                remove = "&#160;";
                index = value.indexOf(remove);
                int len = remove.length();
                
                while (index != -1) {
                        value = value.substring(0, index).concat(
                                        value.substring(index + len, value.length()));
                        index = value.indexOf(remove);
                }
                
                return value;
        }

        public MapPoint[] addPoint(MapPoint[] points) {
                MapPoint[] result = new MapPoint[points.length + 1];
                
                for (int i = 0; i < points.length; i++)
                        result[i] = points[i];
                
                result[points.length] = new MapPoint(0,0);
                
                return result;
        }

        private static String[] split(String strString, String strDelimiter) {
        	
                String[] strArray;
                int iOccurrences = 0;
                int iIndexOfInnerString = 0;
                int iIndexOfDelimiter = 0;
                int iCounter = 0;
                
                if (strString == null) {
                        throw new IllegalArgumentException("Input string cannot be null.");
                }
                
                if (strDelimiter.length() <= 0 || strDelimiter == null) {
                        throw new IllegalArgumentException("Delimeter cannot be null or empty.");
                }
                
                if (strString.startsWith(strDelimiter)) {
                        strString = strString.substring(strDelimiter.length());
                }
                
                if (!strString.endsWith(strDelimiter)) {
                        strString += strDelimiter;
                }
                
                while ((iIndexOfDelimiter = strString.indexOf(strDelimiter, iIndexOfInnerString)) != -1) {
                        iOccurrences += 1;
                        iIndexOfInnerString = iIndexOfDelimiter + strDelimiter.length();
                }
                
                strArray = new String[iOccurrences];
                iIndexOfInnerString = 0;
                iIndexOfDelimiter = 0;
                
                while ((iIndexOfDelimiter = strString.indexOf(strDelimiter, iIndexOfInnerString)) != -1) {
                        strArray[iCounter] = strString.substring(iIndexOfInnerString, iIndexOfDelimiter);
                        iIndexOfInnerString = iIndexOfDelimiter + strDelimiter.length();
                        iCounter += 1;
                }

                return strArray;
        }
        
}
