package com.carbon.tracker.map;

import java.util.LinkedList;
import java.util.List;

import android.location.Address;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.carbon.tracker.entity.MapPoint;
import com.carbon.tracker.entity.Road;
import com.carbon.tracker.gps.GPSManager;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;

/*
 * Objekt v katerem hranimo podatke o aktivnosti.
 */
public class ConfigData {

	public ConfigData(){}
	
	public GPSManager gpsManager;
    public LinearLayout linearLayout;
    public MapView mapView;
    public int drawTime;
    public Address selectedAddress;
    public MapPoint endPoint;
    public MapPoint startPoint;
    public LinkedList<Road> roads;;
	public int runMode;
	public TextView textView;
    public List<Overlay> listOfOverlays;
    public MyLocationOverlay myLocationOverlay;
	
	
}
