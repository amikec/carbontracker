package com.carbon.tracker.map;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.carbon.tracker.Constants;
import com.carbon.tracker.GoogleMapsActivity;
import com.carbon.tracker.R;
import com.carbon.tracker.db.xml.XML;
import com.carbon.tracker.entity.MapPoint;
import com.carbon.tracker.entity.Road;
import com.carbon.tracker.entity.UserSettings;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


/*
 * Metode za pred-računanje podatkov za mapo
 */

public class CompareRoutesAsync extends AsyncTask<Object, Object, Object[]>{

	private Context mainContext;
	private MapPoint start;
	private Dialog dialog;
	private MapPoint end;
	private UserSettings setting;
	private LinkedList<Road> roads = new LinkedList<Road>();
	
	public LinkedList<Road> getRoads(){
		return roads;
	}
	
	public CompareRoutesAsync(Road road){
		road.calculateRoadLength();
		this.roads.add(road);
	}
	
	public CompareRoutesAsync(){}
	
	@Override
	protected void onPostExecute(Object[] result) {
		compareRoutes();
		super.onPostExecute(result);
	}
	
	/*
	 * 	Prikaz dialoga s podatki o poteh
	 */
	private void compareRoutes(){
		
		try{
		
			XML xml = new XML(mainContext);
			
			this.setting = xml.selectSettings();
		}catch(Exception e){
			
			this.setting = new UserSettings();
		}
		
		dialog = new Dialog(mainContext);
		
		dialog.setContentView(R.layout.custom_dialog_compare);
		dialog.setTitle(R.string.route_comparison);
		
		TextView textView = (TextView) dialog.findViewById(R.id.text_view_comparison);
		
		textView.setOnClickListener(onTextViewClick);
		
		ImageView imgButt = (ImageView) dialog.findViewById(R.id.imageButton1);
		imgButt.setOnClickListener(onImageClick);
		
		String comparisonText = "";
		
		for(int i = 0; i < roads.size(); i++){
			Road r = roads.get(i);
			String mode = getVehicleType(r.getMode());
			
			if(Constants.runMode == Constants.START_MODE_TRACK && i == 0)
				mode += " - User";
			
			String distance = "";
			String duration = "";
			String gpkm = String.format("%.2f g/km", getGramsPerKilometer(r));
			
			if(r.getLengthMeters() > 1000)
				distance = String.format("%.2f km", r.getLengthMeters()/1000.0);
			else
				distance = r.getLengthMeters() + " m";
			
			int t = r.getTravelTime();
			int temp = t / 60;
			temp = t - temp*60;
			
			if(t > 3600)
				duration = String.format("\n  Duration: %d h, %d min", t / 3600 , temp);
			else
				duration = String.format("\n  Duration: %d min", t / 60);
				
			if(r.getMode() == Constants.TRAVEL_MODE_BUS){
				duration = "";
				gpkm = String.format("%.2f g/km", getGramsPerKilometer(r)/1000);
			}
			
			String tmp = String.format("  Travel mode: %s"+
					  "\n  Emmisions: %s"+
					  "%s" + 
					  "\n  Distance: %s \n \n", 
					  mode, gpkm, duration, distance);
 
			comparisonText += tmp;
			drawMarker(r, tmp);
			
		}
		textView.setText(comparisonText);
		
		dialog.show();
		
	}
	
	private void drawMarker(Road r, String content) {
		
		MapPoint mp1 = r.getmRoute().get(r.getmRoute().size() / 2);
		
		GeoPoint g = new GeoPoint(mp1.getLatitudeE6(), mp1.getLongitudeE6());
		
		Drawable drawable = mainContext.getResources().getDrawable(R.drawable.marker);
    	ItemizedOverlayImpl itemizedoverlay = new ItemizedOverlayImpl(drawable, mainContext);  
    	OverlayItem overlayitem = new OverlayItem(g, "Road data", content);
    	itemizedoverlay.addOverlay(overlayitem);
    	((GoogleMapsActivity)mainContext).mapView.getOverlays().add(itemizedoverlay);
		
	}

	/*
	 * Pridobimo googlove poti (pes, avto)
	 */
	private void calculateRoutes(MapPoint start, MapPoint end){
		Road roadCar = RoadProvider.getRouteData(start, end, Constants.TRAVEL_MODE_CAR);
		
		String error = "";
		
		if(roadCar.getmRoute().size() > 0)
			roads.add(roadCar);
		else 
			error += "No route available for vehicles. \n";
		
		Road roadPed = RoadProvider.getRouteData(start, end, Constants.TRAVEL_MODE_WALK);
		
		if(roadPed.getmRoute().size() > 0)
			roads.add(roadPed);
		else
			error += "No route available for walking.";
		
		Road roadBus = RoadProvider.getRouteData(start, end, Constants.TRAVEL_MODE_BUS);
		
		if(roadBus.getmRoute().size() > 0)
			roads.add(roadBus);
		else
			error += "No route available for bus transit.";
		
	}
	@Override
	protected Object[] doInBackground(Object... arg0) {
		
		Object[] result = null;
		
		try{
			
			this.mainContext = (Context) arg0[0];
			this.start = (MapPoint) arg0[1];
			this.end = (MapPoint) arg0[2];
			
			if(this.end == null){
				this.end = this.roads.get(0).getmRoute().get(this.roads.get(0).getmRoute().size() - 1);
			}
			
			calculateRoutes(start, end);
			
			result = new Object[]{this.roads};
			
		}catch(Exception e){
			Log.d("error", e.getMessage());
		}
		
		return result;
	}
	@Override
	protected void onCancelled() {
		//dialog.dismiss();
		try {
			CompareRoutesAsync.this.finalize();
		} catch (Throwable e) {
			Log.d("error", e.getMessage());
		}
		super.onCancelled();
	}

	/*
	 * vrne najbližji naslov zadnje koordinate v poti
	 */
	private void getEndLocationFromGeocoder() throws IOException {
		
		Road trackedR = this.roads.getFirst();
		
		try{
			Geocoder geocoder = new Geocoder(mainContext);
			
			MapPoint mp = trackedR.getmRoute().get(trackedR.getmRoute().size() - 1);
			
			List<Address> fromLocation = geocoder.getFromLocation(mp.mLatitude, mp.mLongitude, 10);
			Address addr = fromLocation.get(0);
			
			
			this.end = new MapPoint((int)(addr.getLatitude()*1E6), (int)(addr.getLongitude()*1E6));
		}catch(Exception e){
			
			Log.d("error", e.getMessage());
			this.end = trackedR.getmRoute().get(trackedR.getmRoute().size() - 1);
		}
	}
	
	/*
	 * vrne string glede na travelMode
	 */
	private String getVehicleType(int travelMode){
		
		switch (travelMode){
			case Constants.TRAVEL_MODE_WALK:
				return "Walk";
			case Constants.TRAVEL_MODE_BIKE: 
				return "Bike";
			case Constants.TRAVEL_MODE_CAR:
				return "Car";
			case Constants.TRAVEL_MODE_BUS:
				return "Bus";
			case Constants.TRAVEL_MODE_MOTORBIKE:
				return "Motorbike";
			case Constants.TRAVEL_MODE_STAT:
				return "Stat";
			default:
				return "";
		}
	}
	
	/*
	 * vrne količino onasneževanja za določeno pot glede na travelMode
	 */
	private double getGramsPerKilometer(Road road){
		
		//TODO nardit lookup za avtobus porabo
		double km = road.calculateEmittingLength();
		double gpkm = 0.0;
		
		switch (road.getMode()){
		case Constants.TRAVEL_MODE_BUS:
			gpkm = Constants.BUS_GPKM * km;
			break;
		case Constants.TRAVEL_MODE_MOTORBIKE:
			gpkm = setting.getGramsPerKm() * km;
			break;
		default:
			gpkm = setting.getGramsPerKm() * km;
			break;
		}
		
		return gpkm;
	}
		
	/*
	 * click listener za TextView na dialogu
	 */
	public OnClickListener onTextViewClick = new OnClickListener(){

		public void onClick(View v) {
			//Looper.myLooper().quit();
			dialog.dismiss();
		}		
	};
	public OnClickListener onImageClick = new OnClickListener() {
		
		public void onClick(View arg0) {
			dialog.dismiss();
		}
	};
	
}
