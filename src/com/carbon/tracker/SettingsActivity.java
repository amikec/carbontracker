package com.carbon.tracker;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.carbon.tracker.db.xml.XML;
import com.carbon.tracker.entity.CarbonData;
import com.carbon.tracker.entity.DbSyncXML;
import com.carbon.tracker.entity.Entity;
import com.carbon.tracker.entity.UserSettings;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class SettingsActivity extends Activity {
	
	Dialog dialog;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        ProgressDialog pleaseWaitDialog;
		pleaseWaitDialog = ProgressDialog.show(SettingsActivity.this, "Uploading data", "Please wait");
		pleaseWaitDialog.show();
        ConnectivityManager conMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
    	
    	if(netInfo !=null && netInfo.isConnected()){
    		
    		DbSyncXML bazaSync = new DbSyncXML(SettingsActivity.this);
    		
    		try{
	    		bazaSync.execute();
	    		//TODO pravi url
	    		int id = Constants.user.getUserId();
	    		
	    		XML xml = new XML(SettingsActivity.this);
	    		UserSettings settings = xml.selectSettings();
	    		double gpkm = settings.getGramsPerKm();
	    		
	    		URL url = new URL("http://88.200.24.244/calculateEmissions.php?uID="+id+"&car="+gpkm+"g");
    		    HttpURLConnection httpconn = (HttpURLConnection)url.openConnection();
	    		
    		    StringBuilder response = new StringBuilder();
    		    
    		    if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK)
    		    {
    		        BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()),8192);
    		        String strLine = null;
    		        
    		        while ((strLine = input.readLine()) != null)
    		        {
    		            response.append(strLine);
    		        }
    		        
    	  	        input.close();
    	  	        
    	  	        DbEmissionData getData = new DbEmissionData(SettingsActivity.this);
    	  	        getData.execute();
    	  	        CarbonData data = (CarbonData)getData.get();	
    	  	        finish();
    	  	        showStatistics(data);
    	  	        
    	  	    }else{
    	  	    	Toast.makeText(getApplicationContext(), "No statistics for your profile, sorry :)", Toast.LENGTH_SHORT).show();
    	  	    	finish();
    	  	    }
    		    
    		}catch(Exception e){
    			Toast.makeText(getApplicationContext(), "To slow connection", Toast.LENGTH_SHORT).show();
    			finish();
    		}
    		
    		
    		
    	}else{
    		Toast.makeText(getApplicationContext(), "You have no internet connection", Toast.LENGTH_SHORT).show();
    	}
    	pleaseWaitDialog.dismiss();
	}
	
	private void showStatistics(CarbonData data){
		dialog = new Dialog(this);
		
		dialog.setContentView(R.layout.custom_dialog_compare);
		dialog.setTitle(R.string.route_comparison);
		
		TextView textView = (TextView) dialog.findViewById(R.id.text_view_comparison);
		
		textView.setOnClickListener(onTextViewClick);
		
		ImageView imgButt = (ImageView) dialog.findViewById(R.id.imageButton1);
		imgButt.setOnClickListener(onImageClick);
		
		String statText = "trololol";
		//TODO izpis statistike

		textView.setText(statText);
	}
	
	public OnClickListener onTextViewClick = new OnClickListener(){

		public void onClick(View v) {
			dialog.dismiss();
		}		
	};
	
	public OnClickListener onImageClick = new OnClickListener() {
		
		public void onClick(View arg0) {
			dialog.dismiss();
		}
	};
	
	class DbEmissionData extends AsyncTask<Void, Void, Entity> {
		ProgressDialog pleaseWaitDialog;
		Context kontekst;
		
		public DbEmissionData(Context kon){
			kontekst = kon;
		}
		
		@Override
		protected Entity doInBackground(Void... params) {
			CarbonData cData = new CarbonData();
			cData.select("*", "Users_idUsers = " + Constants.user.getUserId());
			
			return cData;
		}
		
		@Override
		protected void onCancelled() {
			pleaseWaitDialog.dismiss();
			super.onCancelled();
		}
		
		@Override
		protected void onPostExecute(Entity result) {
			pleaseWaitDialog.dismiss();
			super.onPostExecute(result);
		}
		
		@Override
		protected void onPreExecute() {
			pleaseWaitDialog = ProgressDialog.show(kontekst, "Getting data from server", "Please wait");
			pleaseWaitDialog.setOnCancelListener(new OnCancelListener() {
				
				public void onCancel(DialogInterface dialog) {
					DbEmissionData.this.cancel(true);
					finish();
				}
			});
			pleaseWaitDialog.show();
			super.onPreExecute();
			
		}
	}
}