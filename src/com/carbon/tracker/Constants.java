package com.carbon.tracker;

import java.sql.Connection;

import com.carbon.tracker.entity.User;

public class Constants {
	/*
	 * Način zagona aplikacije.
	 */
	public static int runMode;
	public static final int START_MODE_TRACK = 0;
	public static final int START_MODE_DES_LOC = 1;

    //JSON / KML
    public static final String DATA_TYPE = "json";	
	
	/*
	 * Način potovanja. 
	 */
    public static final int TRAVEL_MODE_WALK = 0;
    public static final int TRAVEL_MODE_BIKE = 1;
    public static final int TRAVEL_MODE_CAR = 2;
    public static final int TRAVEL_MODE_BUS = 3;
    public static final int TRAVEL_MODE_MOTORBIKE = 4;
    public static final int TRAVEL_MODE_STAT = 5;
    
    public static final int DRAW_WHOLE_ROUTE = 0;
    public static final int DRAW_TO_POINT = 1;
    
    /*
     * Način izbire končne lokacije
     */
    public static final int LOCATION_ADDRESS = 0;
    public static final int LOCATION_TAP = 1;
    
    /*
     * GPS settings
     */
    public static final int GPS_MIN_TIME = 10000;
    public static final int GPS_MIN_DIST = 0;
    
    /*
     * User data
     */
    public static User user;
	public static int userId;
	public static boolean firstTime = false;
	public static final int SYNC_WIFI = 0;
	public static final int SYNC_INTERVAL = 1;
	public static final int SYNC_ON_END_ROAD = 2;
    public static int drawTime = -1;
    public static int directionsPoints = 0;
    public static final double BUS_GPKM = 54.18;
	
	/*
	 * Files
	 */
	public static final String FILE_DB = "/baza.xml";
	/*
	 *  Database connection
	 */
	public static Connection CONN = null;
	
}
